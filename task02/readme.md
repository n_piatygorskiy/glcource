Task: write a Guess the Number game using the Bash script.\

Subtask: create a random number generator script.\
task02: add random gen script\
Subtask completed\

Subtask: Extract Function refactoring.\
refactoring: add get random function\
refactoring: implement random generation using bc\
Subtask completed\

Subtask: implement comparsion generated random X with Y, which was given as first command line parameter and output next message: Y is less than X, Y is greater/equal to X\
compx: implemented comp X and Y, messages\
Subtask completed\

Subtask: ask user to input Y val if it was not given as parameter\
task02: add X request to script\
Subtask completed\

Subtask: implement posibility input value of top value of random generation diapason as second command line parametr\
task02: add input max range as second param\
Subtask completed\

Subtask: ask user to input maxrange val if is was not given as param\
task02: add maxrange request if omitted\
Sybtask completed\

Subtask: implement posibility to provide amount of guessing tries as third command line parameter\
task02: add tries amount handling\
Subtaqsk completed\

Subtask: ask user a number of tries of it was not provided as third parameter\
task02: add request number of tries if omitted\
Subtask completed\

Subtask: refactoring add coments to main script actions\
task02: subtask comment completion report\
Subtask completed\

Subtask: add posibility to replay with the same parametrs if sucsess guessing\
task02: subtask comment completion report\
Subtask completed\

Note: to run script set ff=unix\
