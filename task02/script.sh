# Printf random number in range specified 1 && 2 params
function print_random () {
  maxr=32767
  a=${1:-0}
  b=${2:-$maxr}
  r=$RANDOM
  r=$(bc <<< "scale=10;$r/$maxr")
  answer=$(bc <<< "scale=0;$r*($b-$a)/1+$a")
  echo $answer
}

function guessgame () {
  declare -i X

  # Request tries amount if omitted 3rd param
  T=$3
  if [ "$T" == "" ]
  then
    echo -n Enter tries amount:
    read T
  fi

  # Request maxrange if omitted 2nd param
  R=$2
  if [ "$R" == "" ]
  then
    echo -n Enter maxrange:
    read R
  fi

  Y=$1

  # loop for multiple tries
  for i in $(seq $T)
  do

  if [ "$Y" == "" ]
  then
    echo -n Enter Y:
    read Y
  fi

  # Request range, if not provided and calculate X
  if [ "$R" != "" ]
  then
    if [[ "$R" -lt "1" || "$R" -gt "100" ]]
    then
      echo Invalid secont parameter range. Must \
      be 1-100
      exit 1
    fi
    maxr=32767
    a=1
    b=$R
    r=$RANDOM
    r=$(bc <<< "scale=10;$r/$maxr")
    X=$(bc <<< "scale=0;$r*($b-$a)/1+$a")
  else
    X=$RANDOM
  fi

  if [[ "$X" -lt "$Y" ]]
  then
    echo "Y is greater than X"
  elif [[ "$X" -gt "$Y" ]]
  then
    echo "Y is less than X"
  else
    echo "Y is equal to X"
    echo -n Play again?[Y]/[N]:
    read choise
    if [ $choise == "Y" ]
    then
      guessgame $Y $R $T
    fi
    exit 0
  fi

  Y=""

  done
}

guessgame $1 $2 $3
