#!/usr/bin/bash
function display_help(){
echo '
Usage: manager [option]
Performs simple file management.

OPTIONS:
        -h --help)  Display this message
        -d <directory>) Set starting directory
        -m <mode>) Mode can be one of the following 
        parameters:
          full/short - show hidden files
          only_dirs - display directories only
          long - long listing format 
          hide_dirs - Hide directories
'
}
function show_dir_nfo(){
clear
echo ">${cur_dir}"
if [[ $f == "" ]]
then
  if [[ $d == "" ]]
  then
    ls $l $m ${cur_dir} | awk '{print NR ". " $0}'
  else
    basename -a `ls -d $m ${cur_dir}/*/` | awk '{print NR ". " $0}'
  fi
else
  if [[ $d == "" ]]
  then
    ls $m $l ${cur_dir} | grep -v ^d |  awk '{print NR ". " $0}'
  fi
fi
}

#Process input parametrs
while [ -n "$(echo $1 | grep '-')" ]; do
	case $1 in
		-h )	display_help
				exit 0
				;;
		--help ) 	dispay_help
				    exit 0
				    ;;
        -d )    if [ -d $2 ]
                then
                    cur_dir=$2
                else
                  echo 'Invalid directory'
                  exit 1
                fi
                ;;
        -m )    case "$2" in
                  full/short)     m="-a"
                                  ;;
                  hide_dirs)      f="f"
                                  ;;
                  only_dirs)      d="d"
                                  ;;
                  long)           l="-l"
                                  ;;
                esac
                shift
                ;;
		 * ) 	echo 'Invalid parameter'
				exit 1
                ;;
	esac
	shift
done

#Main routine
cur_dir=${cur_dir:-$(pwd)}
show_dir_nfo
select option in mode cd cp mv delete leave 
do	
	case "$option" in
	mode)	select mode in full/short only_dirs long hide_dirs
		do	
			case "$mode" in
		    full/short)   if [[ $m == "-a" ]]
                          then
                            m=""
                          else
                            m="-a"
                          fi
                          ;;
			
            hide_dirs)    if [[ $f == "f" ]]
                          then
                            f=""
                          else
                            f="f"
                          fi
                          ;;
            only_dirs)  if [[ $d == "d" ]]
                        then
                          d=""
                        else
                          d="d"
                        fi
                        ;;
            long)       if [[ $l == "-l" ]]
                        then
                          l=""
                        else
                          l="-l"
                        fi
                        ;;
			esac
			break
		done
		;;
	cd )	echo -n \>
		newdir=""
		read newdir
		if [ $newdir == ".." ]
		then
            cur_dir=${cur_dir%/*}
        elif [ $newdir == "." ]
        then
          echo 
		elif [ -d $cur_dir/$newdir ]
		then
			cur_dir=${cur_dir}/${newdir}
		else
			echo "Error directory not exist"
		fi
		;;
    cp)   echo -n Source\>
          read source_dir
          echo -n Dest\>
          read dest_dir
          if [[ -d $source_dir ]] || [[ -f $source_dir ]]
          then
            cp -r $source_dir $dest_dir
          else
            echo Error: invalid target name
          fi
          ;;
    mv)   echo -n Source\>
          read source_dir
          echo -n Dest\>
          read dest_dir
          if [[ -d $source_dir ]] || [[ -f $source_dir ]]
          then
            mv $source_dir $dest_dir
          else
            echo Error: invalid target name
          fi
          ;;
    delete)   echo -n Dest\>
              read dest_dir
              if [[ -d $dest_dir ]] || [[ -f $dest_dir ]]
              then
                rm -r $dest_dir
              else
                echo Error: invalid target name
              fi
              ;;
	leave ) exit 0
		;;
	esac
	
	show_dir_nfo	
	REPLY=
done

