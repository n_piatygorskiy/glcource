#define __BIG_ENDIAN
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/pci.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>

#include <linux/string.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Nikita Piatygorskiy <n_piatygorskiy@knu.ua>");
MODULE_DESCRIPTION("format string to upcase/flip words");
MODULE_VERSION("0.1");

#define MODULE_TAG      "timer_module"
#define PROC_DIRECTORY  "timer"
#define PROC_FILENAME   "buffer"
#define BUFFER_SIZE     100
#define LEN_MSG         100

static char buf_msg[LEN_MSG];
time64_t last_read = 0;

static ssize_t user_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset);
static ssize_t user_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset);
int flip_words(char *str);
void to_upcase(char *str);

static char *proc_buffer;
static size_t proc_msg_length;
static size_t proc_msg_read_pos;

static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file;

static struct proc_ops proc_fops = {
        .proc_read = user_read,
        .proc_write = user_write,
};

static ssize_t user_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset)
{
        size_t left;

        if (last_read != 0) {
                if (strcmp(proc_buffer, "long\n") == 0) {
                        ktime_t hh;
                        ktime_t mm;
                        ktime_t ss;
                        ktime_t now = ktime_get_seconds() - last_read;

                        hh = now / 3600;
                        mm = (now % 3600) / 60;
                        ss = now % 60;

                        printk(KERN_NOTICE MODULE_TAG " time from last read: %2d:%2d:%2d",
                                        hh, mm, ss);
                } else {
                                printk(KERN_NOTICE MODULE_TAG " time from last read: %d",
                                                (ktime_get_seconds() - last_read));
                }
        }

        if (strcmp(proc_buffer, "now\n") == 0) {
                ktime_t now = (ktime_get_real() / 1000000000) % (24 * 3600);
                ktime_t hh;
                ktime_t mm;
                ktime_t ss;

                hh = now / 3600;
                mm = (now % 3600) / 60;
                ss = now % 60;

                printk(KERN_NOTICE MODULE_TAG " time now: %2lu:%2lu:%2lu %lu",
                      hh, mm, ss, now);
        }

        last_read = ktime_get_seconds();

        if (length > (proc_msg_length - proc_msg_read_pos))
                length = (proc_msg_length - proc_msg_read_pos);

        left = copy_to_user(buffer, &proc_buffer[proc_msg_read_pos], length);

        proc_msg_read_pos += length - left;

        if (left)
                printk(KERN_ERR MODULE_TAG "failed to read %u from %u chars\n", left, length);
        else
                printk(KERN_NOTICE MODULE_TAG "read %u chars\n", length);

        return length - left;

        return 0;
}

static ssize_t user_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset)
{
        size_t msg_length;
        size_t left;

        if (length > BUFFER_SIZE)
        {
                printk(KERN_WARNING MODULE_TAG "reduse message length from %u to %u chars\n", length, BUFFER_SIZE);
                msg_length = BUFFER_SIZE;
        }
        else
                msg_length = length;

        left = copy_from_user(proc_buffer, buffer, msg_length);

        proc_msg_length = msg_length - left;
        proc_msg_read_pos = 0;

        if (left)
                printk(KERN_ERR MODULE_TAG "failed to write %u from %u chars\n", left, msg_length);
        else
                printk(KERN_NOTICE MODULE_TAG "written %u chars\n", msg_length);

        return length;

        return length;
}

static ssize_t msg_show(struct class *class, struct class_attribute *attr, char *buf)
{
   strcpy( buf, buf_msg );
   printk( "read %ld\n", (long)strlen( buf ) );
   return 0;
}

static ssize_t msg_store(struct class *class, struct class_attribute *attr, const char *buf, size_t count)
{
   printk( "write %ld\n", (long)count );
   strncpy( buf_msg, buf, count );
   buf_msg[ count ] = '\0';
   return count;
}

struct class_attribute class_attr_conv = __ATTR(conv, (S_IWUSR | S_IRUGO), &msg_show, &msg_store);

static int create_buffer(void)
{
    proc_buffer = kmalloc(BUFFER_SIZE, GFP_KERNEL);
    if (NULL == proc_buffer)
        return -ENOMEM;
    proc_msg_length = 0;

    return 0;
}


static void cleanup_buffer(void)
{
    if (proc_buffer) {
        kfree(proc_buffer);
        proc_buffer = NULL;
    }
    proc_msg_length = 0;
}


static int create_converter_proc(void)
{
    proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
    if (NULL == proc_dir)
        return -EFAULT;

    proc_file = proc_create(PROC_FILENAME, S_IFREG | S_IRUGO | S_IWUGO, proc_dir, &proc_fops);
    if (NULL == proc_file)
        return -EFAULT;

    return 0;
}

static void cleanup_converter_proc(void)
{
    if (proc_file)
    {
        remove_proc_entry(PROC_FILENAME, proc_dir);
        proc_file = NULL;
    }
    if (proc_dir)
    {
        remove_proc_entry(PROC_DIRECTORY, NULL);
        proc_dir = NULL;
    }
}

static struct class *msg_class;

static int simple_init(void)
{
       int err;

        msg_class = class_create(THIS_MODULE, "msg-class");

        if(IS_ERR(msg_class))
                printk("bad class create\n");
        err = class_create_file(msg_class, &class_attr_conv);

        err = create_buffer();
        if (err)
                goto error;

        err = create_converter_proc();
        if (err)
                 goto error;

        printk(KERN_NOTICE MODULE_TAG "loaded\n");
        return 0;

error:
        printk(KERN_ERR MODULE_TAG "failed to load\n");
        cleanup_converter_proc();
        cleanup_buffer();
        return err;
}

static void simple_cleanup(void)
{
        cleanup_converter_proc();
        class_remove_file( msg_class, &class_attr_conv );
        class_destroy( msg_class );
        cleanup_buffer();
        printk(KERN_NOTICE MODULE_TAG "exited\n");
}

module_init(simple_init);
module_exit(simple_cleanup);
