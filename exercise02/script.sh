dir=$1
if [ "$dir" != "" ]
then
  for f in $(dir)/*
  do
    f=$(basename $f)
    if [ "$(find "$dir" -mtime +30 -type f -name "$f" 2>/dev/null)" ]
    then
      touch "$dir/$f"
      mv -- "$dir/$f" "$dir/~$f"
    fi
  done
else
  echo Provide a valid directory name
fi
