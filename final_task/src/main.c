#include "main.h"
#include "ili9341.h"
#include "engine.h"

unsigned int update_speed = 0;

unsigned int GameOver;

int main() 
{
	lcd_init();
	lcd_fill_screen(COLOR_BLACK);
	lcd_update_screen();

	game.state = GameInit();
	unsigned long cycle = 0;

	struct timespec spec;
	clock_gettime(CLOCK_REALTIME, &spec);
	long ns = spec.tv_nsec;
	float sub;

	while (game.state == STATE_RUN) {
#ifdef DEBUG
		clear();
		printf("cycles: %d"
				"fps: %f\n"
				"projectiles count: %d\n"
				"tanks count: %d",
				cycle++, sub,
				game.projectiles_count,
				game.tanks_count);
#endif

		if (GameController() != 0)
			game.state = STATE_ERROR;

		if (update_speed > SCREEN_UPDATE_SPEED_MAX) {
			update_speed = 0;
			lcd_update_screen();

			clock_gettime(CLOCK_REALTIME, &spec);
			sub = (1.0 / ((spec.tv_nsec - ns) * 1e-9));
			if (sub < 0)
				sub = -sub;
			ns = spec.tv_nsec;

		} else 
			update_speed++;
	}

	lcd_update_screen();	
	lcd_free();
}
