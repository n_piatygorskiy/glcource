#include "projectile.h"
#include "lcd.h"
#include "engine.h"
#include "image.h"
#include <stdlib.h>
#include "ili9341.h"

struct Bitmap *projectile_img;

/*Change projectile pos, depends on it prev pos,
 * process interactions with other objcts.
 */
int MoveProjectile(struct PROJECTILE *prj)
{
	//destroy if out of map
	if (prj->map_pos.x >= game.map_width
			|| prj->map_pos.y >= game.map_heigth) {
		prj->health = 0;
	}
	if (prj->pos.x <= 4 && prj->pos_prev.x >= 4) {
		prj->health = 0;
		prj->map_pos.x = 0;
	}
	if (prj->pos.y <= 4 && prj->pos_prev.y >= 4) {
		prj->health = 0;
		prj->map_pos.y = 0;
	}

	//process object hit
	char c;
	if (prj->health > 0)
		if ((c = game.map[prj->map_pos.y][prj->map_pos.x]) != 'g'
				&& c != '*') {
			if (c >= '0' && c <= '7') {
				int tph = c - '0' + 1;
				tph -= prj->health;
				if (tph - 1 < 0)
					tph = 0;
				prj->health -= c - '0' + 1 - tph;
				c = tph - 1 + '0';
				if (c < '0')
					game.map[prj->map_pos.y][prj->map_pos.x] = 'g';
				else
					game.map[prj->map_pos.y][prj->map_pos.x] = c;
			} else
				prj->health = 0;
		}

	// Process interaction with tanks
	for (int i = 0; i < game.tanks_count; i++) {
		// We need to redraw tank ?
		if (abs(game.tanks[i]->pos.x - prj->pos_prev.x) <= 30
		  && abs(game.tanks[i]->pos.y - prj->pos_prev.y) <= 30)
			game.tanks[i]->redraw_flag = 2;

		// Collision with tank
		if (abs(game.tanks[i]->pos.x - prj->pos_prev.x) <= 10
		  && abs(game.tanks[i]->pos.y - prj->pos_prev.y) <= 10
		  && i != prj->owner) {
			int tph = game.tanks[i]->health;
			tph -= prj->health;
			if (tph < 0)
				tph = 0;
			prj->health -= game.tanks[i]->health - tph;
			game.tanks[i]->health = tph;
		}
	}


	//move
	if (prj->health > 0) {
		prj->pos_prev = prj->pos;
		switch (prj->direction) {
		case DIRECT_DOWN:
			if (Map2LcdPos(prj->map_pos).y <= prj->pos.y)
				prj->map_pos.y++;
			prj->pos.y+=4;
			break;
		case DIRECT_LEFT:
			if (Map2LcdPos(prj->map_pos).x >= prj->pos.x
					&& prj->map_pos.x != 0)
				prj->map_pos.x--;
			prj->pos.x -= 4;
			break;
		case DIRECT_UP:
			if (Map2LcdPos(prj->map_pos).y >= prj->pos.y
					&& prj->map_pos.y != 0)
				prj->map_pos.y--;
			prj->pos.y -= 4;
			break;
		case DIRECT_RT:
			if (Map2LcdPos(prj->map_pos).x <= prj->pos.x)
				prj->map_pos.x++;
			prj->pos.x += 4;
			break;
		default:
			return 1;
		}
	}

	return 0;
}

int DrawProjectile(struct PROJECTILE *prj)
{
	struct COORD pos = prj->map_pos;

	//restore other objects
	if (Map2LcdPos(prj->map_pos).x > prj->pos_prev.x) {
		pos.x--;
		RedrawSquare(pos.x, pos.y);
	} else if (Map2LcdPos(prj->map_pos).x < prj->pos_prev.x) {
		pos.x++;
		RedrawSquare(pos.x, pos.y);
	} else if (Map2LcdPos(prj->map_pos).y > prj->pos_prev.y) {
		pos.y--;
		RedrawSquare(pos.x, pos.y);
	} else if (Map2LcdPos(prj->map_pos).y < prj->pos_prev.y) {
		pos.y++;
		RedrawSquare(pos.x, pos.y);
	}

	pos = prj->map_pos;
	if (pos.x < game.map_width && pos.y < game.map_heigth)
		RedrawSquare(pos.x, pos.y);
	
	if (prj->health == 0) {
		prj->destroyed = 1;
		return 0;
	}

	//Draw projectile at new pos:
	struct Rectangle img_part;
	img_part.y = 2;
	img_part.w = 20;
	img_part.h = 20;

	pos = prj->pos;
	pos.x -= game.frame.x * 20;
	pos.y -= game.frame.y * 20;
	switch (prj->type) {
	case STANDART:
		switch (prj->direction) {
		case DIRECT_UP:
			img_part.x = 2 + 24 * 2;
			break;
		case DIRECT_DOWN:
			img_part.x = 2;
			break;
		case DIRECT_LEFT:
			img_part.x = 2 + 24;
			break;
		case DIRECT_RT:
			img_part.x = 2 + 24 * 3;
			break;
		}

		DrawImg(projectile_img, pos, img_part, COLOR_WHITE);	
		break;
	case PLASMA:
		switch (prj->direction) {
		case DIRECT_UP:
			img_part.x = 2 + 24 * 6;
			break;
		case DIRECT_DOWN:
			img_part.x = 2 + 24 * 4;
			break;
		case DIRECT_LEFT:
			img_part.x = 2 + 24 * 5;
			break;
		case DIRECT_RT:
			img_part.x = 2 + 24 * 7;
			break;
		}
		DrawImg(projectile_img, pos, img_part, COLOR_WHITE);	
		break;
	default:
		return 1;
	}

	return 0;
}
