#ifndef ENGINE_H
#define ENGINE_H

#include "tank.h"
#include "projectile.h"
#include "main.h"

// Keyboard check speed
#define CHECK_SPD_MAX 1

int GameController();

int GameInit();

struct COORD Map2LcdPos(struct COORD map_pos);

int RedrawSquare(int x, int y);

int CheckPosition(struct COORD pos);
//==============================Game Objects====================================

extern struct Bitmap *wall_conc_img;
extern struct Bitmap *walls_img;

enum gamestate {
	STATE_OVER,
	STATE_RUN,
	STATE_SUSPENDED,
	STATE_ERROR,
};

#define FRAME_WIDTH LCD_WIDTH / 20
#define FRAME_HEIGHT LCD_HEIGHT / 20

/*describes which part of game.map will be 
 *redrawn on lcd
 */
struct FRAME {
	uint16_t x;
	uint16_t y;
	uint16_t w;
	uint16_t h;
};

/*Temporary add pack couse strange 
 * bug with gcc in TankShoot func
 */
#pragma pack(push, 1)
extern struct GAME {
	char **map;
	struct FRAME frame;

	//represent all tanks
	struct TANK **tanks;
	uint16_t tanks_count;

	//represent all projectiles
	struct PROJECTILE **projectiles;
	int projectiles_count;

	uint16_t map_width;
	uint16_t map_heigth;
	uint16_t player_tank;
	enum gamestate state;
} game;
#pragma pop()

//=============================================================================

#endif
