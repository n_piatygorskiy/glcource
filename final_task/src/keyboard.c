#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

#include "lcd.h"
#include "keyboard.h"

unsigned int flags[256];

static int gpio_set_value(unsigned int gpio, unsigned int value)
{
	int fd;
	char buf[BUF_SIZE];

	snprintf (buf, sizeof(buf), GPIO_DIR "/gpio%d/value", gpio);

	fd = open (buf, O_WRONLY);
	if (fd < 0) {
		perror ("gpio/set-value");
		return fd;
	}

	if (value) {
		write (fd, "1", 2);
	}
	else {
		write (fd, "0", 2);
	}

	close (fd);
 	return 0;
}

static int gpio_get_value(unsigned int gpio)
{
	int fd;
	char buf[BUF_SIZE];
	char value_buf[3];

	snprintf (buf, sizeof(buf), GPIO_DIR "/gpio%d/value", gpio);

	fd = open (buf, O_RDONLY);
	if (fd < 0) {
		perror ("gpio/get-value");
		return fd;
	}	

	if (-1 == read(fd, value_buf, 3)) {
		perror ("Failed to read value");
		return -1;
	}

	close (fd);
 	return (atoi(value_buf));
}

int KeyboardInit()
{
	memset(flags, 0, 256 * sizeof(unsigned));

	if (gpio_init_out(KB_L_1) != 0)
		return 1;

	if (gpio_init_out(KB_L_2) != 0)
		return 1;

	if (gpio_init_out(KB_L_3) != 0)
		return 1;

	if (gpio_init_out(KB_L_4) != 0)
		return 1;

	if (gpio_init_in(KB_C_1) != 0)
		return 1;

	if (gpio_init_in(KB_C_2) != 0)
		return 1;

	if (gpio_init_in(KB_C_3) != 0)
		return 1;

	if (gpio_init_in(KB_C_4) != 0)
		return 1;

	return 0;
}

int KeyboardFree()
{
	gpio_free(KB_L_1);
	gpio_free(KB_L_2);
	gpio_free(KB_L_3);
	gpio_free(KB_L_4);
	gpio_free(KB_C_1);
	gpio_free(KB_C_2);
	gpio_free(KB_C_3);
	gpio_free(KB_C_4);
}

/* Case we use driver we didnt need that
 */
static void read_line(int line, char *character)
{
	gpio_set_value(line, 0);	

	if (gpio_get_value(KB_C_1) != 1)
		flags[character[0]]++;
	else 
		flags[character[0]] = 0;

	if (gpio_get_value(KB_C_2) != 1)
		flags[character[1]]++;
	else 
		flags[character[1]] = 0;

	if (gpio_get_value(KB_C_3) != 1)
		flags[character[2]]++;
	else 
		flags[character[2]] = 0;
	
	if (gpio_get_value(KB_C_4) != 1)
		flags[character[3]]++;
	else 
		flags[character[3]] = 0;

	gpio_set_value(line, 1);	
}

void KeyboardCheck()
{
	FILE *keyboard;
	
	memset(flags, 0, sizeof(flags));

	keyboard = fopen(KB_PROC_ENTRY, "r");
	if (keyboard == NULL) {
#ifdef DEBUG
		printf("KeyboardCheck: fopen error, "
				"maybe you forget insert a driver\n");
		return;
#endif
	}

	char c = fgetc(keyboard);
	if (c >= 0)
		flags[c] = 1;

	fclose(keyboard);
}
