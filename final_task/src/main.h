#ifndef MAIN_H
#define MAIN_H

#define PROJ_DIR "/home/pi/glcource/final_task/"
#define RES_DIR PROJ_DIR "res/"
#define BYTE_SIZE 8
#define SCREEN_UPDATE_SPEED_MAX 5

extern unsigned int update_speed;

#define DEBUG

#ifdef DEBUG
#include <stdio.h>
#include <time.h>

#define clear() printf("\033[H\033[J")

#endif

/*indicates, does game continue*/
extern unsigned GameOver;

/*Object directions*/
enum directions {
	DIRECT_DOWN,
	DIRECT_LT_DN_1,
	DIRECT_LT_DN_2,
	DIRECT_LT_DN_3,
	DIRECT_LEFT,
	DIRECT_LT_UP_1,
	DIRECT_LT_UP_2,
	DIRECT_LT_UP_3,
	DIRECT_UP,
	DIRECT_UP_RT_1,
	DIRECT_UP_RT_2,
	DIRECT_UP_RT_3,
	DIRECT_RT,
	DIRECT_RT_DN_1,
	DIRECT_RT_DN_2,
	DIRECT_RT_DN_3,
};

#endif
