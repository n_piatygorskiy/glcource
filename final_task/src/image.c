#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "main.h"
#include "ili9341.h"
#include "lcd.h"
#include "image.h"

/*fill pixels array with line-by-libe rotated raw image data
 *from imageFile
 */
static void bmp_to_array(char *pixels, FILE *imageFile, struct Bitmap *bmp)
{
	struct BITMAPINFOHEADER *hdr = &bmp->dib_header;

	unsigned int unpaddedRowSize = 
		hdr->img_width * hdr->bits_per_pixel / BYTE_SIZE;
	unsigned int paddedRowSize = 
		hdr->img_size / hdr->img_height;

	char *rowptr = pixels+((hdr->img_height-1)*unpaddedRowSize);

	for (int i = 0; i < hdr->img_height; i++)
	{
		fseek(imageFile, bmp->offset_img + (i*paddedRowSize), 
				SEEK_SET);
		if(fread(rowptr, 1, unpaddedRowSize, imageFile) < 1) {
#ifdef DEBUG
			printf("bmp_to_array: fread error\n");
#endif
			break;
		}
		rowptr -= unpaddedRowSize;
	}
}

#ifdef DEBUG
void print_header(struct Bitmap *bmp)
{
	printf("File size	:	%d\n", bmp->file_size);
	printf("Img offset	:	%d\n", bmp->offset_img);
	printf("DIB header\n");
	printf("	Header size	:	%u\n",bmp->dib_header.header_size);
	printf("	Image width	:	%u\n",bmp->dib_header.img_width);
	printf("	Image height:	%u\n",bmp->dib_header.img_height);
	printf("	Color plahes:	%hu\n",bmp->dib_header.color_planes);
	printf("	Color depth	:	%hu\n",bmp->dib_header.bits_per_pixel);
	printf("	Compression	:	%u\n",bmp->dib_header.compression);
	printf("	Image size	:	%u\n",bmp->dib_header.img_size);
	printf("	Horis resol	:	%u\n",bmp->dib_header.h_resolution);
	printf("	Vert resol	:	%u\n",bmp->dib_header.v_resolution);
	printf("	Colors num	:	%u\n",bmp->dib_header.n_colors);
	printf("	Colors used	:	%u\n",bmp->dib_header.n_color_used);
}
#endif

/*allocate space for bmp struct and fill it with with
 *bmp image data taken from file
 *	@filename - name of bitmap file, stored in 
 *	RES_DIR folder
 */
struct Bitmap *ReadImg(const char *filename)
{
	FILE *file;
	char fullpath[100];
	struct Bitmap *bmp;

	sprintf(fullpath, RES_DIR "%s", filename);
	file = fopen(fullpath, "r");
	if (file == NULL) {
#ifdef DEBUG
		printf("read_img: could not open " 
				RES_DIR "%s\n", filename);
#endif
		return NULL;
	}

	bmp = malloc(sizeof(struct Bitmap));	
	if (bmp == NULL) {
#ifdef DEBUG
		printf("read_img: alloc error\n");
#endif
		return NULL;
	}

	if (fread(bmp, 1, sizeof(*bmp), file) < 1) {
#ifdef DEBUG
		printf("read_img: fread error\n");
#endif
		return NULL;
	}

	char *pixels = malloc(bmp->dib_header.img_size);
	if (pixels == NULL) {
#ifdef DEBUG
		printf("read_img: malloc error\n");
#endif
		return NULL;
	}

	if (memset(pixels, 0, bmp->dib_header.img_size) == NULL) {
#ifdef DEBUG
		printf("read_img: memset error\n");
#endif
		return NULL;
	}

	bmp_to_array(pixels, file, bmp);
	bmp->pixels = pixels;

	fclose(file);

	return bmp;
}

/*draw part bitmap image on lcd display, check for inavlid img size
 *and draw position
 *	@bmp - must be filled with img data,
 *	using proc ReadImg
 *	@left_upper - coordinates of img
 *	left upper corner 
 *	@i_p - describes which part of image may be drawn
 *	@filter_color - bmp color which will be filtered
 */
int DrawImg(struct Bitmap *bmp, 
		struct COORD left_upper, 
		struct Rectangle i_p, 
		uint16_t filter_color)
{
	int h, w, x0, y0;
	char *pixels;
	h = bmp->dib_header.img_height;
	w = bmp->dib_header.img_width;
	x0 = left_upper.x;
	y0 = left_upper.y;
	pixels = bmp->pixels;

	if (i_p.y < 0 || i_p.y + i_p.h > h) {
#ifdef DEBUG
		printf("DrawImg: wrong size\n");
#endif
		return 1;
	}
	if (i_p.x < 0 || i_p.x + i_p.w > w) {
#ifdef DEBUG
		printf("DrawImg: wrong size\n");
#endif
		return 1;
	}

	for (uint16_t i = i_p.y; i < i_p.h + i_p.y; i++) {
		for (uint16_t j = i_p.x; j < i_p.w + i_p.x; j += 1) {
			uint16_t color = COLOR_COLOR565(pixels[i*w*3+j*3],
				       	pixels[i*w*3+j*3+1],
					pixels[i*w*3+j*3+2]);

			if (color != filter_color)
				lcd_draw_pixel(x0+j-i_p.x, y0+i-i_p.y, color);
		}
	}

	return 0;
}


