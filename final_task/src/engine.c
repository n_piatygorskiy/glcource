#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"

#include "engine.h"
#include "ili9341.h"
#include "keyboard.h"
#include "image.h"

struct GAME game;

struct Bitmap *walls_img;
struct Bitmap *ground_img;

/*Check, is specified position free of blocks,
 *tanks, turrets, spikes, etc.
 */
int CheckPosition(struct COORD pos)
{
	if (pos.x < 0 || pos.y < 0
			|| pos.x >= game.map_width
			|| pos.y >= game.map_heigth)
		return 0;

	if (game.map[pos.y][pos.x] != 'g')
		return 0;

	for (int i = 0; i < game.tanks_count
			&& game.tanks[i] != NULL; i++)	
		if (game.tanks[i]->map_pos.x == pos.x
				&& game.tanks[i]->map_pos.y == pos.y)
			return 0;
	return 1;
}

/* Redraw one Square plate 20x20 size,
 * depends on game.map matrix, which
 * must be filled with correct map part
 *	@x - x pos on game.map
 *	@y - y pos on game.map
 */
int RedrawSquare(int x, int y)
{
	struct COORD draw_pos;
	struct Rectangle img_part;

	//not draw square out of frame
	if (x < game.frame.x || x > game.frame.x + game.frame.w)
		return 0;
	if (y < game.frame.y || y > game.frame.y + game.frame.h)
		return 0;

	img_part.w = 20;
	img_part.h = 20;
	draw_pos.x = (x - game.frame.x) * 20;
	draw_pos.y = (y - game.frame.y) * 20;

	switch(game.map[y][x]) {
	case 'g': {
			img_part.x = 0;
			img_part.y = 0;
			if (DrawImg(ground_img,
				draw_pos,
				img_part,
				COLOR_WHITE) != 0)
				return 1;
		} break;
	case '9': {
			img_part.x = 9 * 20;
			img_part.y = 0;
			if (DrawImg(walls_img,
				draw_pos,
				img_part,
				COLOR_WHITE) != 0)
				return 1;
		} break;
	case '7': {	
			img_part.x = 0;
			img_part.y = 0;
			if (DrawImg(walls_img,
				draw_pos,
				img_part,
				COLOR_WHITE) != 0)
				return 1;
		} break;
	case '6': {	
			img_part.x = 20;
			img_part.y = 0;
			if (DrawImg(walls_img,
				draw_pos,
				img_part,
				COLOR_WHITE) != 0)
				return 1;
		} break;
	case '5': {	
			img_part.x = 2*20;
			img_part.y = 0;
			if (DrawImg(walls_img,
				draw_pos,
				img_part,
				COLOR_WHITE) != 0)
				return 1;
		} break;
	case '4': {	
			img_part.x = 3*20;
			img_part.y = 0;
			if (DrawImg(walls_img,
				draw_pos,
				img_part,
				COLOR_WHITE) != 0)
				return 1;
		} break;
	case '3': {	
			img_part.x = 4*20;
			img_part.y = 0;
			if (DrawImg(walls_img,
				draw_pos,
				img_part,
				COLOR_WHITE) != 0)
				return 1;
		} break;
	case '2': {	
			img_part.x = 5*20;
			img_part.y = 0;
			if (DrawImg(walls_img,
				draw_pos,
				img_part,
				COLOR_WHITE) != 0)
				return 1;
		} break;
	case '1': {	
			img_part.x = 6*20;
			img_part.y = 0;
			if (DrawImg(walls_img,
				draw_pos,
				img_part,
				COLOR_WHITE) != 0)
				return 1;
		} break;
	case '0': {	
			img_part.x = 7*20;
			img_part.y = 0;
			if (DrawImg(walls_img,
				draw_pos,
				img_part,
				COLOR_WHITE) != 0)
				return 1;
		} break;
	case '*': {	
			img_part.x = 2*20;
			img_part.y = 0;
			if (DrawImg(walls_img,
				draw_pos,
				img_part,
				COLOR_WHITE) != 0)
				return 1;
		} break;

	default: {
#ifdef DEBUG
			 puts("RedrawSquare: invalid map");
#endif
			 return 1;
		 } 
	}

	return 0;
}

/*draw all walls and ground on map
 */
static int DrawMap()
{
	for (int y = game.frame.y; y < game.frame.y + game.frame.h; y++)
		for (int x = game.frame.x; x < game.frame.x + game.frame.w; x++)
			if (RedrawSquare(x, y) != 0)
				return 1;

	for (int i = 0; i < game.tanks_count; i++) {
		if (!game.tanks[i]->destroyed)
			RedrawTank(i);
	}

	return 0;
}

/*Reset current draw-frame position if needed.
 */
static void ResetFrame()
{
	struct TANK *tnk = game.tanks[game.player_tank];

	/*case tank is over frame by Ox ot approaching to
	 * frame, center frame on tank
	 */
	if (game.frame.x >= tnk->map_pos.x
			|| game.frame.x + game.frame.w <= tnk->map_pos.x
			|| tnk->map_pos.x - game.frame.x <= 4
			|| game.frame.x + game.frame.w - tnk->map_pos.x <= 4) {
		if (tnk->map_pos.x <= game.frame.w / 2) {
			if (game.frame.x != 0) {
				game.frame.x = 0;
				DrawMap();
			}
		} else if (tnk->map_pos.x + game.frame.w / 2 > game.map_width) {
			if (game.frame.x != game.map_width - game.frame.w) {
				game.frame.x = game.map_width - game.frame.w;
				DrawMap();
			}
		} else {
			game.frame.x = tnk->map_pos.x - game.frame.w / 2;
			DrawMap();
		}
	}
	
	/*Case tank is over frame by Oy or aproaching to
	 *frame, center frame on tank
	 */
	if (game.frame.y >= tnk->map_pos.y
			|| game.frame.y + game.frame.h <= tnk->map_pos.y
			|| tnk->map_pos.y - game.frame.y <= 4
			|| game.frame.y + game.frame.h - tnk->map_pos.y <= 4) {
		if (tnk->map_pos.y <= game.frame.h / 2) {
			if (game.frame.y != 0) {
				game.frame.y = 0;
				DrawMap();
			}
		} else if (tnk->map_pos.y + game.frame.h / 2 > game.map_heigth) {
			if (game.frame.y != game.map_heigth - game.frame.h) {
				game.frame.y = game.map_heigth - game.frame.h;
				DrawMap();
			}
		} else {
			game.frame.y = tnk->map_pos.y - game.frame.h / 2;
			DrawMap();
		}
	}
}

#define MAX_LINE 1000
static int ReadMap(char *filename)
{
	FILE *file;
	char fullpath[100];
	char line[MAX_LINE];
	struct Bitmap *bmp;

	sprintf(fullpath, RES_DIR "%s", filename);
	file = fopen(fullpath, "r");
	if (file == NULL) {
#ifdef DEBUG
		printf("ReadMap: could not open " 
				RES_DIR "%s\n", filename);
#endif
		return 1;
	}

	if (game.map == NULL)
		game.map = calloc(100, sizeof(char *));

	while (fgets(line, MAX_LINE, file) != NULL) {
		if (strcmp(line, "<map>\n") == 0)
			while (fgets(line, MAX_LINE, file) != NULL && strcmp(line, "</map>\n")) {
				int len;
				//Remove redutant newl char
				if (line[strlen(line) - 1] == '\n')
					line[strlen(line) - 1] = '\0';

				if (game.map_width == 0)
					game.map_width = len = strlen(line);
				if (len == 0 || (strlen(line) != game.map_width)) {
#ifdef DEBUG
					printf("ReadMap: invalid map");
#endif
					return 1;
				}
				game.map[game.map_heigth] = malloc(game.map_width + 1);
				strcpy(game.map[game.map_heigth++], line);
			}
	}
	fclose(file);

	return 0;
}

struct COORD Map2LcdPos(struct COORD map_pos)
{
	map_pos.x *= 20;
	map_pos.y *= 20;
	return map_pos;
}

static int DrawEvent()
{
	ResetFrame();
	for (int i = 0; i < game.tanks_count; i++)
	{
		struct TANK *tnk = game.tanks[i];
		if (!tnk->destroyed  && (Changed(i) || tnk->redraw_flag
				|| tnk->health == 0))	
			RedrawTank(i);
		if (game.tanks[i]->redraw_flag > 0)
			game.tanks[i]->redraw_flag--;
	}

	for (int i = 0; i < game.projectiles_count; i++)
	{
		if (game.projectiles[i]->destroyed == 0)
			DrawProjectile(game.projectiles[i]);
	}

	return 0;
}

static int StepEvent()
{
	for (int i = 0; i < game.tanks_count; i++)
		if (!game.tanks[i]->destroyed)
			MoveTank(i);

	for (int i = 0; i < game.projectiles_count; i++)
		if (game.projectiles[i]->health != 0)
			if (MoveProjectile(game.projectiles[i]) != 0)
				game.state = STATE_OVER;

	return 0;
}

/*Process player controls
 */
static void PlayerEvent()
{
	int pl = game.player_tank;

	struct COORD  ppos = game.tanks[pl]->map_pos;

	if (flags['2'] != 0) {
		ppos.y -= 1;
		//player want to move up, could he?
		if (game.tanks[pl]->direction_map == DIRECT_UP) {
			if (CheckPosition(ppos))
				game.tanks[pl]->map_pos = ppos;
		}
			else 
				game.tanks[pl]->direction_map = DIRECT_UP;
	} else if (flags['6']) {
		ppos.x += 1;
		//player want to move right, could he?
		if (game.tanks[pl]->direction_map == DIRECT_RT) {
			if (CheckPosition(ppos))
				game.tanks[pl]->map_pos = ppos;
		}
			else 
				game.tanks[pl]->direction_map = DIRECT_RT;
	} else if (flags['8']) {
		ppos.y += 1;
		//player want to move down, could he?
		if (game.tanks[pl]->direction_map == DIRECT_DOWN) {
			if (CheckPosition(ppos))
				game.tanks[pl]->map_pos = ppos;
		}
			else 
				game.tanks[pl]->direction_map = DIRECT_DOWN;
	} else if (flags['4']) {
		ppos.x -= 1;
		//player want to move left, could he?
		if (game.tanks[pl]->direction_map == DIRECT_LEFT) {
			if (CheckPosition(ppos))
				game.tanks[pl]->map_pos = ppos;
		}
			else 
				game.tanks[pl]->direction_map = DIRECT_LEFT;
	} else if (flags['A']
			&& game.tanks[pl]->shoot_spd_counter >= TANK_SHOOT_SPD_MAX) {
		game.tanks[pl]->shoot_spd_counter = 0;
		TankShoot(pl);
	};
}

int kbd_check_spd_count; 

/*Process all entities decisions
 */
static int KeyEvent()
{
	if (kbd_check_spd_count >= CHECK_SPD_MAX) {
		kbd_check_spd_count = 0;
		KeyboardCheck();
	} else 
		kbd_check_spd_count++;

	if (flags['D'] != 0)
		game.state = STATE_OVER;

	//For all tanks in game
	for (int i = 0; i < game.tanks_count; i++) {

		if (game.tanks[i]->shoot_spd_counter++ < TANK_SHOOT_SPD_MAX)
			game.tanks[i]->shoot_spd_counter++;

		//Tank can accept command now
		if (!game.tanks[i]->destroyed && !Changed(i)) {
			if (game.player_tank == i)
				PlayerEvent();
			else
				TankEvent(i);
		}

		if (game.tanks[i]->destroyed && i == game.player_tank)
			game.state = STATE_OVER;
	}

	return 0;
}

/*Read all needed images from bmp files
 * and place them in structures Bitmap
 */
static int ImagesInit()
{
	if (tank_light_img == NULL)
		tank_light_img = ReadImg("tank_light.bmp");
	if (tank_light_img == NULL) {
#ifdef DEBUG
		puts("Imagesinit: failed read img tank_light.bmp");
#endif
		return 1;
	}

	if (walls_img == NULL)
		walls_img = ReadImg("walls.bmp");
	if (walls_img == NULL) {
#ifdef DEBUG
		puts("ImagesInit: failed read img walls.bmp");
#endif
		return 1;
	}

	if (ground_img == NULL)
		ground_img = ReadImg("ground.bmp");
	if (ground_img == NULL) {
#ifdef DEBUG
		puts("ImagesInit: failed read img ground.bmp");
#endif
		return 1;
	}

	if (health_yell_img == NULL)
		health_yell_img = ReadImg("health_yell.bmp");
	if (health_yell_img == NULL) {
#ifdef DEBUG
		puts("ImagesInit: failed read img health_yell.bmp");
#endif
		return 1;
	}

	if (health_red_img == NULL)
		health_red_img = ReadImg("health_red.bmp");
	if (health_red_img == NULL) {
#ifdef DEBUG
		puts("ImagesInit: failed read img health_red.bmp");
#endif
		return 1;
	}

	if (projectile_img == NULL)
		projectile_img = ReadImg("bullets.bmp");
	if (projectile_img == NULL) {
#ifdef DEBUG
		puts("ImagesInit: failed red img bullets.bmp");
#endif
		return 1;
	}

	if (expl_big_img == NULL)
		expl_big_img = ReadImg("expl_big.bmp");
	if (expl_big_img == NULL) {
#ifdef DEBUG
		puts("ImagesInit: failed red img bullets.bmp");
#endif
		return 1;
	}
	return 0;
}
int GameController()
{
	if (StepEvent() != 0)
		return 1;

	if (DrawEvent() != 0)
		return 1;

	if (KeyEvent() != 0)
		return 1;

	return 0;
}

int GameInit()
{
	game.tanks_count = 6;

	if (ImagesInit() != 0)
		return STATE_ERROR;

	game.tanks = calloc(game.tanks_count, sizeof(struct TANK*));
	if (game.tanks == NULL) {
#ifdef DEBUG
		printf("GameInit: calloc error\n");
#endif
		return STATE_ERROR;
	}

	//write player tank number in GAME
	if (game.tanks_count > 0)
		game.player_tank = 0;

	//map initilization
	game.map_width = 0;
	game.map_heigth = 0;
	if (ReadMap("map1.map") != 0)
		return STATE_ERROR;
	game.frame.w = FRAME_WIDTH;
	game.frame.h = FRAME_HEIGHT;

	//Allocate space for all tanks obj in game
	for (int i = 0; i < game.tanks_count; i++) {
		game.tanks[i] = calloc(1, sizeof(struct TANK));

		if (game.tanks[i] == NULL) {
#ifdef DEBUG
			puts("GameInit: malloc error");
#endif
			return STATE_ERROR;
		}
		if (i == game.player_tank)
			TankInit(i, TEAM_GREEN);
		else TankInit(i, TEAM_RED);
	}	

	game.projectiles = calloc(1000, sizeof(struct PROJECTILE*));
	game.projectiles_count = 0;

	//Drawing objects in frame
	ResetFrame();
		
	if (DrawMap() != 0)
		return STATE_ERROR;	


	for (int i = 0; i < game.tanks_count; i++)
	{
		RedrawTank(i);
	}
	
	return STATE_RUN;
}


