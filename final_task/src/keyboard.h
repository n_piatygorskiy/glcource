#ifndef KEYBOARD_H
#define KEYBOARD_H

#define KB_PROC_ENTRY "/proc/matrix/buffer"

/*Keyboard collumn N gpio number*/
#define KB_C_1 6
#define KB_C_2 13
#define KB_C_3 19
#define KB_C_4 26

/*Keyboard line N gpio number*/
#define KB_L_1 16
#define KB_L_2 20
#define KB_L_3 21
#define KB_L_4 5

/*procedure KeyboardCheck store here character
 * states: 0 - not pushed, 0 > - pushed*/
extern unsigned flags[];

int KeyboardInit();

void KeyboardCheck();

int KeyboardFree();

#endif
