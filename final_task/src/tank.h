#ifndef TANK_H
#define TANK_H

#include "projectile.h"

/*determine, how many game cycles is needed
 *to move/rotate tank on one pxl/pi/8 correspondly
 *Hint:it will be much smarter to move tank depends
 *on timer, not on cycling
 */
#define TANK_SPD_MAX 3
#define TANK_MAX_ROTATE_SPEED 10
#define TANK_STEP_SIZE 1
#define TANK_SHOOT_SPD_MAX 200
#define BLOW_MAX_STEP 12
#define MAX_BLOW_SPD 2

enum TEAM {
	TEAM_GREEN,
	TEAM_RED,
};

extern struct Bitmap *tank_light_img;
extern struct Bitmap *health_yell_img;
extern struct Bitmap *health_red_img;
extern struct Bitmap *expl_big_img;

#pragma pack(push, 1)
struct TANK {
	//Pos on Lcd in pixels
	struct COORD pos;
	//Does we need to redraw tank?
	struct COORD pos_prev;
	//Logical pos on game map in 20x20 squares
	struct COORD map_pos;

	uint16_t direction;
	//Redraw?
	uint16_t direction_prev;
	uint16_t direction_map;

	uint16_t speed;
	uint16_t rotate_speed;

	int health;
	int health_max;
	int destroyed;
	int blow_step;

	enum PROJ_TYPE proj_type; 
	uint16_t shoot_spd_counter;

	int redraw_flag;

	uint16_t team;

	struct Bitmap *img;
};
#pragma pop()

int Changed(int i);

void TankInit(int n, enum TEAM preset);

void RedrawTank(int i);

void MoveTank(int i);

void TankEvent(int n);

void TankShoot(int n);

#endif
