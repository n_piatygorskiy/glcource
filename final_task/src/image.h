#ifndef IMAGE_H
#define IMAGE_H

#include "lcd.h"
/*stores windows compatibe bitmap header 
 */
#pragma pack(push, 1)
struct BITMAPINFOHEADER {
	unsigned int 		header_size;
	unsigned int 		img_width;
	unsigned int 		img_height;
	unsigned short int 	color_planes;
	unsigned short int	bits_per_pixel;
	unsigned int		compression;
	unsigned int		img_size;
	int			h_resolution;
	int			v_resolution;
	unsigned int		n_colors;
	unsigned int		n_color_used;
};

struct Bitmap {
	char BM[2];
	int file_size;
	char data[4];
	int offset_img;
	struct BITMAPINFOHEADER dib_header;
	char *pixels;
};
#pragma pop()

struct Bitmap *ReadImg(const char *filename);

int DrawImg(struct Bitmap *bmp,
		struct COORD left_upper,
		struct Rectangle img_part,
		uint16_t filter_color);

#endif
