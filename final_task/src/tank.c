#include <stdlib.h>
#include "main.h"
#include "ili9341.h"

#include "image.h"
#include "tank.h"
#include "engine.h"

struct Bitmap *tank_light_img;
struct Bitmap *health_yell_img;
struct Bitmap *health_red_img;
struct Bitmap *expl_big_img;

int Changed(int i)
{
	if (game.tanks[i]->pos.x != game.tanks[i]->pos_prev.x)
		return 1;

	if (game.tanks[i]->pos.y != game.tanks[i]->pos_prev.y)
		return 1;

	if (game.tanks[i]->direction != game.tanks[i]->direction_prev)
		return 1;

	if (game.tanks[i]->speed != 0 
			|| game.tanks[i]->rotate_speed !=0) {
		return 1;
	}

	return 0;
}

/*
 * 	@n - tank number in game structure
 */
void TankInit(int n, enum TEAM preset)
{
	struct TANK *tnk = game.tanks[n];
	struct COORD temp;
	time_t rand_seed;

	//initilize with free random position
	srand((unsigned) time(&rand_seed));
	int pos_occupied = 0;
	do {
		pos_occupied = 0;
		temp.x = rand() % game.map_width;
		temp.y = rand() % game.map_heigth;
		if (!CheckPosition(temp))
			pos_occupied = 1;
	} while (pos_occupied);

	tnk->map_pos = temp;
	tnk->pos = Map2LcdPos(temp);
	tnk->pos_prev = Map2LcdPos(temp);
	tnk->direction = DIRECT_UP;
	tnk->direction_prev = DIRECT_UP;
	tnk->direction_map = DIRECT_UP;

	tnk->speed = 0;
	tnk->rotate_speed = 0;
	tnk->img = tank_light_img; 
	tnk->health = 10; 
	tnk->health_max = 10; 
	tnk->destroyed = 0;
	tnk->team = preset;

	tnk->proj_type = PLASMA;
}

int blow_spd_count;

void RedrawTank(int i)
{
	struct TANK *t = game.tanks[i];
	struct COORD pos = t->map_pos;
	struct Rectangle img_part;
	float hp_part;

	//restore other objects
	RedrawSquare(t->map_pos.x, t->map_pos.y);	
	if (Map2LcdPos(t->map_pos).x > t->pos_prev.x) {
		pos.x--;
		RedrawSquare(pos.x, pos.y - 1);
	} else if (Map2LcdPos(t->map_pos).x < t->pos_prev.x) {
		pos.x++;
		RedrawSquare(pos.x, pos.y - 1);
	} else if (Map2LcdPos(t->map_pos).y > t->pos_prev.y) {
		pos.y--;
		RedrawSquare(pos.x, pos.y - 1);
	} else if (Map2LcdPos(t->map_pos).y < t->pos_prev.y) {
		pos.y++;
	}
	RedrawSquare(pos.x, pos.y);


	
	// Destroy tank
	if (t->health == 0) {
		// Draw explosion
		if (t->blow_step < BLOW_MAX_STEP) {
			// Clear health
			RedrawSquare(pos.x, pos.y - 1);

			pos = t->pos;
			pos.x -= game.frame.x * 20;
			pos.y -= game.frame.y * 20;		
			img_part.y = 5;
			img_part.x = t->blow_step * 30 + 5;
			img_part.w = 20;
			img_part.h = 20;
			
			DrawImg(expl_big_img, pos, img_part, COLOR_WHITE);	
			if (blow_spd_count >= MAX_BLOW_SPD) {
				blow_spd_count = 0;
				t->blow_step++;
			} else 
				blow_spd_count++;
		} else {
			t->destroyed = 1;
			t->map_pos.x = game.map_width;
			t->map_pos.y = game.map_heigth;
		}
		return;
	}	

	//draw tank img
	pos = t->pos;
	pos.x -= game.frame.x * 20;
	pos.y -= game.frame.y * 20;
	img_part.y = 2;
	img_part.x = t->direction * 24 + 2;
	img_part.w = 20;
	img_part.h = 20;
	DrawImg(tank_light_img, pos, img_part, COLOR_WHITE);	

	//draw tank helth 
	img_part.x = 2;
	hp_part = (float)t->health / t->health_max; 
	float temp = 1.0 / 12;
	while (temp + 0.1 / 12 < hp_part) {
		temp += 1.0 / 12;
		img_part.x += 24;
	}

	pos.x = t->pos.x - game.frame.x * 20;
	pos.y = t->pos.y - 4 - game.frame.y * 20;
	img_part.y = 0;
	img_part.w = 20;
	img_part.h = 4;

	struct Bitmap *health_bar;
	if (t->team = TEAM_GREEN)
		health_bar = health_yell_img;
	else
		health_bar = health_red_img;

	DrawImg(health_bar, pos, img_part, COLOR_WHITE);

	// Set redraw flags for close tanks
	for (int j = 0; j < game.tanks_count; j++) {
		if (i != j && abs(game.tanks[j]->pos.x - t->pos_prev.x) <= 30
		  && abs(game.tanks[j]->pos.y - t->pos_prev.y) <= 30)
			game.tanks[j]->redraw_flag = 1;
	}

}

void TankEvent(int n)
{
	struct TANK *tnk = game.tanks[n];
	struct COORD ppos = game.tanks[n]->map_pos;
	time_t rand_seed;
	uint16_t direction = tnk->direction_map;

	// Can tank shoot player?
	struct COORD player_pos = game.tanks[game.player_tank]->pos;
	struct COORD tank_pos = game.tanks[n]->pos;

	int dir_tar = -1;
	if (abs(player_pos.x - tank_pos.x) < 20
				&& abs(tank_pos.y - player_pos.y) <= 120) {
		if (tank_pos.y < player_pos.y)
			dir_tar = DIRECT_DOWN;
		else
			dir_tar = DIRECT_UP;
	}
	if (abs(tank_pos.y - player_pos.y) < 20
				&& abs(tank_pos.x - player_pos.x) <= 120) {
		if (tank_pos.x < player_pos.x)
			dir_tar = DIRECT_RT;
		else
			dir_tar = DIRECT_LEFT;
	}
	
	// Choose random direction
	if (dir_tar == -1) {
		uint16_t rnd = rand() % 16;
		switch (rnd) {
		case 0:
			direction = DIRECT_DOWN;
			break;
		case 1:
			direction = DIRECT_UP;
			break;
		case 2:
			direction = DIRECT_LEFT;
			break;
		case 3:
			direction = DIRECT_RT;
			break;
		}
	} 
	
	if (dir_tar == direction) {
		if(tnk->shoot_spd_counter >= TANK_SHOOT_SPD_MAX) {
			tnk->shoot_spd_counter = 0;
			TankShoot(n);
		}
		tnk->shoot_spd_counter++;
		return;
	} else 
		if (dir_tar != -1)
			direction = dir_tar;

	if (direction == DIRECT_UP) {
		ppos.y -= 1;
		//tank want to move up, could he?
		if (game.tanks[n]->direction_map == DIRECT_UP) {
			if (CheckPosition(ppos))
				game.tanks[n]->map_pos = ppos;
		} else 
			game.tanks[n]->direction_map = DIRECT_UP;
	} else if (direction == DIRECT_RT) {
		ppos.x += 1;
		//tank want to move right, could he?
		if (game.tanks[n]->direction_map == DIRECT_RT) {
			if (CheckPosition(ppos))
				game.tanks[n]->map_pos = ppos;
		} else 
			game.tanks[n]->direction_map = DIRECT_RT;
	} else if (direction == DIRECT_DOWN) {
		ppos.y += 1;
		//tank want to move down, could he?
		if (game.tanks[n]->direction_map == DIRECT_DOWN) {
			if (CheckPosition(ppos))
				game.tanks[n]->map_pos = ppos;
		} else 
			game.tanks[n]->direction_map = DIRECT_DOWN;
	} else if (direction == DIRECT_LEFT) {
		ppos.x -= 1;
		//tank want to move left, could he?
		if (game.tanks[n]->direction_map == DIRECT_LEFT) {
			if (CheckPosition(ppos))
				game.tanks[n]->map_pos = ppos;
		} else 
			game.tanks[n]->direction_map = DIRECT_LEFT;
	}
}

/*Changes real position/direction of tank, depends
 * on target position/direction
 * 	@i - tank number in game.tanks
 */
void MoveTank(int i)
{
	struct TANK *t = game.tanks[i];
	struct COORD pos = Map2LcdPos(t->map_pos);

	//Move tank up/right/down/left
	if (pos.x != t->pos.x || pos.y != t->pos.y) {
		//check speed counter
		if (t->speed >= TANK_SPD_MAX) {
			t->speed = 0;
			if (pos.x < t->pos.x) {
				t->pos_prev.x = t->pos.x;
				t->pos.x -= TANK_STEP_SIZE;	
			} else if (pos.x > t->pos.x) {
				t->pos_prev.x = t->pos.x;
				t->pos.x += TANK_STEP_SIZE;	
			} else if (pos.y < t->pos.y) {
				t->pos_prev.y = t->pos.y;
				t->pos.y -= TANK_STEP_SIZE;
			} else if (pos.y > t->pos.y) {
				t->pos_prev.y = t->pos.y;
				t->pos.y += TANK_STEP_SIZE;
			}
		} else 
			t->speed++;
	} else
		t->pos_prev = t->pos;
	//Rotate tank
	if (t->direction != t->direction_map) {
		//speed check
		if (t->rotate_speed >= TANK_MAX_ROTATE_SPEED) {
			t->rotate_speed = 0;
			t->direction_prev = t->direction;
			if (t->direction > t->direction_map)
				if (t->direction - t->direction_map > 8) {
					t->direction++;
					if (t->direction > 15)
						t->direction = 0;
				} else 
					t->direction--;
			else 
				if (t->direction_map - t->direction > 8) {
					if (t->direction == 0)
						t->direction = 15;
					else
						t->direction--;
				} else
					t->direction++;
		} else
			t->rotate_speed++;

	} else
		t->direction_prev = t->direction;
}

void TankShoot(int n)
{
	struct TANK *tnk = game.tanks[n];
	int new = game.projectiles_count;

	//search for free projectile struct
	for (int i = 0; i < game.projectiles_count; i++) {
		if (game.projectiles[i]->destroyed == 1) {
			new = i;
			break;
		}
	}
	
	if (game.projectiles[new] == NULL) {
		game.projectiles[new] = malloc(sizeof(struct PROJECTILE));
		game.projectiles_count++;
	}

	struct PROJECTILE *prj = game.projectiles[new];

	switch (tnk->proj_type) {
	case STANDART: 
		prj->health = 1;
		prj->type = STANDART;
		break;
	case PLASMA:
		prj->health = 2;
		prj->type = PLASMA;
		break;
	}

	prj->owner = n;

	//set prj position
	prj->destroyed = 0;
	prj->map_pos = tnk->map_pos;
	prj->pos = tnk->pos;
	prj->pos_prev = tnk->pos_prev;
	prj->direction = tnk->direction;
}
