#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "lcd.h"
#include "main.h"

enum PROJ_TYPE {
	STANDART,
	PLASMA,
	DOUBLE_STANDART,
	DOUBLE_PLASMA,
	armor_piercing,
	powered_armor_piercing,
	rocket,
	controlled_rocket,
};

#pragma pack(push, 1)
struct PROJECTILE {
	/*determine, how much damage can it deal
	 *until dissapear */
	int health;
	int destroyed;
	int owner;
	struct COORD map_pos;
	struct COORD pos;
	//in redrawing needs
	struct COORD pos_prev;
	enum directions direction;
	enum PROJ_TYPE type;
};
#pragma pop()

extern struct Bitmap *projectile_img;

int MoveProjectile(struct PROJECTILE *prj);

int DrawProjectile(struct PROJECTILE *prj);

#endif
