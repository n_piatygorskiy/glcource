#ifndef LCD_H
#define LCD_H

#include <stdint.h>
#include "fonts.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0])) 
#define GPIO_PIN_RESET	LCD_PIN_RESET 
#define GPIO_PIN_DC	LCD_PIN_DC 
#define GPIO_PIN_CS	LCD_PIN_CS 
#define GPIO_DIR		"/sys/class/gpio" 
#define BUF_SIZE	64 
#define BTN_UP 18 
#define BTN_MID	23 
#define BTN_DOWN 24 

/*Saves buttons current state to struct btn_state, * current state to xn_prev */ 
/*Saves button state: current and previos value.
 * we need prev value to determine when btn whas re-pressed
 */
struct ButtonState{
	unsigned int x1;
	unsigned int x1_prev;
	unsigned int x2;
	unsigned int x2_prev;
	unsigned int x3;
	unsigned int x3_prev;
};


void lcd_set_address_window(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1);

void lcd_write_data(uint8_t *buff, unsigned long buff_size);

void check_button(struct ButtonState *btn_state); 

void lcd_put_char(uint16_t x, uint16_t y, char ch, FontDef font, uint16_t color, uint16_t bgcolor); 

void lcd_draw_pixel(uint16_t x, uint16_t y, uint16_t color);

void lcd_init();

int lcd_free();

int gpio_init_out(unsigned int gpio);

int gpio_init_in(unsigned int gpio); 

int gpio_free(unsigned int gpio);

void lcd_fill_screen(uint16_t color);

void lcd_fill_rectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color);

void lcd_update_screen(void);

struct Rectangle {
	uint16_t	x; 
	uint16_t	y; 
	uint16_t	w;
	uint16_t 	h;
};

struct TextBox{
	struct Rectangle	rect;
	uint16_t	text_color;
	uint16_t	text_bgcolor;
	FontDef		*font;
	uint16_t 	bgcolor;
	uint16_t	border_size;
	uint16_t	border_color;
};

//Print in rectangle box

void draw_box(struct TextBox *box);

void lcd_printf(struct TextBox *box, char *szFormat, ...); 

void lcd_put_string(struct TextBox *box, char *szString);

struct COORD {
	uint16_t x;
	uint16_t y;
};

#endif
