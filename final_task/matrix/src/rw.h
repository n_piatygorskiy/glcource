int procfs_init(void);
void procfs_exit(void);

extern size_t proc_msg_length;
extern size_t proc_msg_read_pos;

extern char *proc_buffer;
