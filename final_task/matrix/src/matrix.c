#include <linux/init.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/gpio.h>

#include "rw.h"
#include "matrix.h"

#define MATRIX_PIN_X1 6
#define MATRIX_PIN_X2 13
#define MATRIX_PIN_X3 19
#define MATRIX_PIN_X4 26
#define MATRIX_PIN_Y1 16
#define MATRIX_PIN_Y2 20
#define MATRIX_PIN_Y3 21
#define MATRIX_PIN_Y4 5

unsigned int irq_num1, irq_num2, irq_num3, irq_num4;

static void read_line(int line, char *c)
{
	char res = 0;
	gpio_set_value(line, 0);
	mdelay(1);

	if (gpio_get_value(MATRIX_PIN_X1) != 1)
		res = c[0];

	if (gpio_get_value(MATRIX_PIN_X2) != 1)
		res = c[1];

	if (gpio_get_value(MATRIX_PIN_X3) != 1)
		res = c[2];

	if (gpio_get_value(MATRIX_PIN_X4) != 1)
		res = c[3];

	if (res != 0) {
		proc_msg_read_pos = 0;
		proc_msg_length = 1;
		proc_buffer[0] = res;
	}

	gpio_set_value(line, 1);
}

void check_matrix()
{
	gpio_set_value(MATRIX_PIN_Y1, 1);
	gpio_set_value(MATRIX_PIN_Y2, 1);
	gpio_set_value(MATRIX_PIN_Y3, 1);
	gpio_set_value(MATRIX_PIN_Y4, 1);
	mdelay(1);

	read_line(MATRIX_PIN_Y1, "123A");
	read_line(MATRIX_PIN_Y2, "456B");
	read_line(MATRIX_PIN_Y3, "789C");
	read_line(MATRIX_PIN_Y4, "*0#D");

	gpio_set_value(MATRIX_PIN_Y1, 0);
	gpio_set_value(MATRIX_PIN_Y2, 0);
	gpio_set_value(MATRIX_PIN_Y3, 0);
	gpio_set_value(MATRIX_PIN_Y4, 0);
}

static int __init matrix_init(void)
{
	int ret;

	procfs_init();

	ret = gpio_request(MATRIX_PIN_X1, "MATRIX_PIN_X1");
	if (ret) {
		printk(KERN_ERR "matrix: failed to request GPIO%d: %d\n", MATRIX_PIN_X1, ret);
		return -1;
	}

	ret = gpio_request(MATRIX_PIN_X2, "MATRIX_PIN_X2");
	if (ret) {
		printk(KERN_ERR "matrix: failed to request GPIO%d: %d\n", MATRIX_PIN_X2, ret);
		return -1;
	}

	ret = gpio_request(MATRIX_PIN_X3, "MATRIX_PIN_X3");
	if (ret) {
		printk(KERN_ERR "matrix: failed to request GPIO%d: %d\n", MATRIX_PIN_X3, ret);
		return -1;
	}

	ret = gpio_request(MATRIX_PIN_X4, "MATRIX_PIN_X4");
	if (ret) {
		printk(KERN_ERR "matrix: failed to request GPIO%d: %d\n", MATRIX_PIN_X4, ret);
		return -1;
	}

	ret = gpio_request(MATRIX_PIN_Y1, "MATRIX_PIN_Y1");
	if (ret) {
		printk(KERN_ERR "matrix: failed to request GPIO%d: %d\n", MATRIX_PIN_Y1, ret);
		return -1;
	}

	ret = gpio_request(MATRIX_PIN_Y2, "MATRIX_PIN_Y2");
	if (ret) {
		printk(KERN_ERR "matrix: failed to request GPIO%d: %d\n", MATRIX_PIN_Y2, ret);
		return -1;
	}

	ret = gpio_request(MATRIX_PIN_Y3, "MATRIX_PIN_Y3");
	if (ret) {
		printk(KERN_ERR "matrix: failed to request GPIO%d: %d\n", MATRIX_PIN_Y3, ret);
		return -1;
	}

	ret = gpio_request(MATRIX_PIN_Y4, "MATRIX_PIN_Y4");
	if (ret) {
		printk(KERN_ERR "matrix: failed to request GPIO%d: %d\n", MATRIX_PIN_Y4, ret);
		return -1;
	}

	gpio_direction_output(MATRIX_PIN_Y1, 0);
	gpio_direction_output(MATRIX_PIN_Y2, 0);
	gpio_direction_output(MATRIX_PIN_Y3, 0);
	gpio_direction_output(MATRIX_PIN_Y4, 0);
	gpio_direction_input(MATRIX_PIN_X1);
	gpio_direction_input(MATRIX_PIN_X2);
	gpio_direction_input(MATRIX_PIN_X3);
	gpio_direction_input(MATRIX_PIN_X4);

	gpio_set_value(MATRIX_PIN_Y1, 0);
	gpio_set_value(MATRIX_PIN_Y2, 0);
	gpio_set_value(MATRIX_PIN_Y3, 0);
	gpio_set_value(MATRIX_PIN_Y4, 0);
	msleep(1);

	printk("gpio x1: %d x2: %d x3: %d x4: %d",
			gpio_get_value(MATRIX_PIN_X1),
			gpio_get_value(MATRIX_PIN_X2),
			gpio_get_value(MATRIX_PIN_X3),
			gpio_get_value(MATRIX_PIN_X4));
	
	printk(KERN_INFO "matrix: module loaded\n");
	return 0;
}

static void __exit matrix_exit(void)
{
	procfs_exit();
	
	gpio_free(MATRIX_PIN_X1);
	gpio_free(MATRIX_PIN_X2);
	gpio_free(MATRIX_PIN_X3);
	gpio_free(MATRIX_PIN_X4);
	gpio_free(MATRIX_PIN_Y1);
	gpio_free(MATRIX_PIN_Y2);
	gpio_free(MATRIX_PIN_Y3);
	gpio_free(MATRIX_PIN_Y4);

	printk(KERN_INFO "matrix: module exited\n");
}

module_init(matrix_init);
module_exit(matrix_exit);

MODULE_AUTHOR("Nikita Piatygorskiy");
MODULE_DESCRIPTION("Matrix Keyboard");
MODULE_LICENSE("GPL");
