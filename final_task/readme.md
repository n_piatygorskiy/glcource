# Author
- Nikita Piatygorskiy
- Kiev
# University
  Shevchenko, faculty of cybernetics and computere science, 3rd - cource, informatics
# Title
Tanchiki demo
# Description
1. ...
2. Equipment: raspberry pi zero, ili9341 display, 4x4 matrix keyboard, sd-card
# Used software
- OS Raspbian
- gcc
- vim
- git
- Make
- gdb
- and more...
# Cource themes used in project
- git
- chardev
- time
- memory
- fs
# Troubles 
- Matrix keyboard spamming interrupts
# Demonstrarion
 Video in folder ./video
## References
