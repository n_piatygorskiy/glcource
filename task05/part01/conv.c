#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/string.h>

#define COURCE 31

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Nikita");
MODULE_DESCRIPTION("Valute converter.");
MODULE_VERSION("0.01");

static int val = 0;
module_param(val, int, 0660);

static char *type = "";
module_param(type, charp, 0660);


static int __init conv_init(void)
{
	int ival, fval;
	if (strcmp(type, "grn") == 0) {
		ival = val * 100;
		ival /= COURCE;
		fval = ival % 100;
		ival /= 100;
		printk(KERN_INFO "%d grn is %d.%d eur\n", val, ival, fval);
	}
	else if (strcmp(type, "eur") == 0)
		printk(KERN_INFO "%d eur is %d grn\n", val, val * COURCE);	
	else 
		printk(KERN_INFO "Invalid type\n");
	return 0;
}

static void __exit conv_exit(void)
{
	printk(KERN_INFO "Converter unloaded\n");
}

module_init(conv_init);
module_exit(conv_exit);
