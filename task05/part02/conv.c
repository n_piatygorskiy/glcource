#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/pci.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>

#include <linux/string.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Nikita Piatygorskiy <n_piatygorskiy@knu.ua>");
MODULE_DESCRIPTION("format string to upcase/flip words");
MODULE_VERSION("0.1");

#define MODULE_TAG      "converter_module "
#define PROC_DIRECTORY  "converter"
#define PROC_FILENAME   "buffer"
#define BUFFER_SIZE     200
#define LEN_MSG         100

static char buf_msg[LEN_MSG];

struct CURRENCY {
        int val_i;      //integer part
        int val_f;      //float part
};
static int cource_eur_to_grn = 31;;

struct CURRENCY to_eur(struct CURRENCY v_in);
struct CURRENCY to_grn(struct CURRENCY v_in);
int atoi(const char *st);

static ssize_t user_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset);
static ssize_t user_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset);

static char *proc_buffer;
static size_t proc_msg_length;
static size_t proc_msg_read_pos;

static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file;

static struct proc_ops proc_fops = {
        .proc_read = user_read,
        .proc_write = user_write,
};
static int write_flag;
static ssize_t user_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset)
{
        size_t left;
        struct CURRENCY val_input,val_grn,val_eur;

        if (!write_flag) {
                write_flag = 1;
                val_input.val_i = atoi(&proc_buffer[proc_msg_read_pos]);
                val_input.val_f = 0;    //Atoi may be upgraded to read float values also

                val_grn = to_grn(val_input);
                val_eur = to_eur(val_input);

                proc_msg_length = sprintf(&proc_buffer[proc_msg_read_pos],"%d.%d grn is %d.%d eur\n"
                                "%d.%d eur is %d.%d grn",
                                val_input.val_i, val_input.val_f,
                                val_eur.val_i, val_eur.val_f,
                                val_input.val_i, val_input.val_f,
                                val_grn.val_i, val_grn.val_f);
                proc_msg_read_pos = 0;
        }

        printk(KERN_NOTICE MODULE_TAG "buf: %s, len: %d", proc_buffer, length);

        if (length > (proc_msg_length - proc_msg_read_pos))
                length = (proc_msg_length - proc_msg_read_pos);

        left = copy_to_user(buffer, &proc_buffer[proc_msg_read_pos], length);

        proc_msg_read_pos += length - left;

        if (left)
                printk(KERN_ERR MODULE_TAG "failed to read %u from %u chars\n", left, length);
        else
                printk(KERN_NOTICE MODULE_TAG "read %u chars\n", length);

        return length - left;
}

static ssize_t user_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset)
{
        size_t msg_length;
        size_t left;
        write_flag = 0;

        if (length > BUFFER_SIZE)
        {
                printk(KERN_WARNING MODULE_TAG "reduse message length from %u to %u chars\n", length, BUFFER_SIZE);
                msg_length = BUFFER_SIZE;
        }
        else
                msg_length = length;

        left = copy_from_user(proc_buffer, buffer, msg_length);

        proc_msg_length = msg_length - left;
        proc_msg_read_pos = 0;

        if (left)
                printk(KERN_ERR MODULE_TAG "failed to write %u from %u chars\n", left, msg_length);
        else
                printk(KERN_NOTICE MODULE_TAG "written %u chars\n", msg_length);

        return length;
}

static ssize_t msg_show(struct class *class, struct class_attribute *attr, char *buf)
{
   strcpy( buf, buf_msg );
   printk( "read %ld\n", (long)strlen( buf ) );
   return 0;
}

static ssize_t msg_store(struct class *class, struct class_attribute *attr, const char *buf, size_t count)
{
        printk( "write %ld\n", (long)count );
        strncpy( buf_msg, buf, count );
        buf_msg[ count ] = '\0';

        cource_eur_to_grn = atoi(buf_msg);

        return count;
}

struct class_attribute class_attr_conv = __ATTR(eur_to_grn, (S_IWUSR | S_IRUGO), &msg_show, &msg_store);

static int create_buffer(void)
{
    proc_buffer = kmalloc(BUFFER_SIZE, GFP_KERNEL);
    if (NULL == proc_buffer)
        return -ENOMEM;
    proc_msg_length = 0;

    return 0;
}


static void cleanup_buffer(void)
{
    if (proc_buffer) {
        kfree(proc_buffer);
        proc_buffer = NULL;
    }
    proc_msg_length = 0;
}


static int create_converter_proc(void)
{
    proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
    if (NULL == proc_dir)
        return -EFAULT;

    proc_file = proc_create(PROC_FILENAME, S_IFREG | S_IRUGO | S_IWUGO, proc_dir, &proc_fops);
    if (NULL == proc_file)
        return -EFAULT;

    return 0;
}

static void cleanup_converter_proc(void)
{
    if (proc_file)
    {
        remove_proc_entry(PROC_FILENAME, proc_dir);
        proc_file = NULL;
    }
    if (proc_dir)
    {
        remove_proc_entry(PROC_DIRECTORY, NULL);
        proc_dir = NULL;
    }
}

static struct class *msg_class;

static int simple_init(void)
{
       int err;

        msg_class = class_create(THIS_MODULE, "currency");

        if(IS_ERR(msg_class))
                printk("bad class create\n");
        err = class_create_file(msg_class, &class_attr_conv);

        err = create_buffer();
        if (err)
                goto error;

        err = create_converter_proc();
        if (err)
                 goto error;

        printk(KERN_NOTICE MODULE_TAG "loaded\n");
        return 0;

error:
        printk(KERN_ERR MODULE_TAG "failed to load\n");
        cleanup_converter_proc();
        cleanup_buffer();
        return err;
}

static void simple_cleanup(void)
{
        cleanup_converter_proc();
        class_remove_file( msg_class, &class_attr_conv );
        class_destroy( msg_class );
        cleanup_buffer();
        printk(KERN_NOTICE MODULE_TAG "exited\n");
}

module_init(simple_init);
module_exit(simple_cleanup);

struct CURRENCY to_eur(struct CURRENCY v_in)
{
        v_in.val_i *= 100;
        v_in.val_i /= cource_eur_to_grn;
        v_in.val_f = v_in.val_i % 100;
        v_in.val_i /= 100;
        return v_in;
}
struct CURRENCY to_grn(struct CURRENCY v_in)
{
        v_in.val_i *= 100;
        v_in.val_i += v_in.val_f;
        v_in.val_i *= cource_eur_to_grn;
        v_in.val_f = v_in.val_i % 100;
        v_in.val_i /= 100;
        return v_in;
}

int atoi(const char *st)
{
        int isdigit(int c);
        int pos = 0, res = 0;
        char digit;

        while(isdigit(digit = st[pos++]))
                res = res*10 + digit - '0';

        return res;
}

int isdigit(int c)
{
        return c >= '0' && c <= '9';
}
