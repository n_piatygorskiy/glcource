dispay_help(){
echo 'Usage: script2 [OPTION]... [DIRECTORY]...
Remove files, which ends with .tmp or begins with -, _, ~ in DIRECTORY. By default remove in done in current directory.
	-h, --help		Dispaly that message
	-r, --recursive	Perform deletion in all subdirectories recursively, remove empty directories.
	-y, --yes		Never prompt
	-t, --test		Dispaly path of files, which may be deleted, but not remove them.'
}
while [ -n "$(echo $1 | grep '-')" ]; do
	case $1 in
		-h )		dispay_help
				exit 0
				;;
		--help ) 	dispay_help
				exit 0
				;;
		-r ) 		r=" " 
				;;
		--recursive )	r=" "
				;;	
		-y )		echo Not yet
                                ;;
		-t ) 		t=" "
				p="-print"
				;;
		--test )	t=" "
				p="-print"
				;;
		 * ) 		echo 'Invalid parameter'
				exit 1
	esac
	shift
done
dir="${1:-$PWD}"
level="${r:-"-maxdepth 1"}"
delete="${t:-"-delete"}"
find $dir $level -name '*.tmp' $p $delete
find $dir $level -name '[~_-]*' $p $delete
find $dir $level -type d -empty $delete
