#include <time.h>
#include "lcd.h"
#include "ili9341.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>

#define UPDATE_SPEED 1

void get_local_ip(char *szip);

int main(int argc, char *argv[])
{
	lcd_init();

	lcd_fill_screen(COLOR_BLACK);
	lcd_update_screen();

	gpio_init_in(BTN_UP);
	gpio_init_in(BTN_MID);
	gpio_init_in(BTN_DOWN);

	struct ButtonState btn_state;

	struct TextBox counter_box;
	counter_box.rect.x = LCD_WIDTH / 2;
       	counter_box.rect.y = LCD_HEIGHT / 2;
	counter_box.rect.w = 50;
	counter_box.rect.h = Font_11x18.height;
	counter_box.text_bgcolor = COLOR_BLACK;
	counter_box.text_color = COLOR_WHITE;
	counter_box.bgcolor = COLOR_BLACK;
	counter_box.font = &Font_11x18;

	struct TextBox time_box;
	time_box.rect.x = LCD_WIDTH - 8 * Font_11x18.width;
       	time_box.rect.y = 0;
	time_box.rect.w = 8 * Font_11x18.width + 100;
	time_box.rect.h = Font_11x18.height;
	time_box.text_bgcolor = COLOR_BLACK;
	time_box.text_color = COLOR_WHITE;
	time_box.bgcolor = COLOR_BLACK;
	time_box.font = &Font_11x18;

	struct TextBox ip_box;
	ip_box.rect.x = 0;
       	ip_box.rect.y = LCD_HEIGHT - Font_11x18.height;
	ip_box.rect.w = 15 * Font_11x18.width;
	ip_box.rect.h = Font_11x18.height;
	ip_box.text_bgcolor = COLOR_BLACK;
	ip_box.text_color = COLOR_WHITE;
	ip_box.bgcolor = COLOR_BLACK;
	ip_box.font = &Font_11x18;

	int flag_exit = 1, counter = 0, spd_count = 0;
	unsigned int time_btnx2_pressed = 0;
	time_t time_box_time;
	struct tm *tm_time;
	char ip[25];

	while (flag_exit) {
		check_button(&btn_state);

		//Middle button pressed or unpressed	
		if (btn_state.x2_prev != btn_state.x2) {
			if (btn_state.x2 == 1)
				time_btnx2_pressed = time(NULL);
			else {
				if (time(NULL) - time_btnx2_pressed <= 1)
					counter = 0;
			}
		}

		//X1 button pressed
		if (btn_state.x1_prev == 0 && btn_state.x1 == 1) {
			counter++;
		}

		//X3 button pressed
		if (btn_state.x3_prev == 0 && btn_state.x3 == 1) {
			counter--;
		}

		//How long middle button being pressed
		if (btn_state.x2 == 1 && (time(NULL) - time_btnx2_pressed >= 2))
			flag_exit = 0; 	//Quit if > 2s

		lcd_printf(&counter_box, "%d", counter);

		time(&time_box_time);
		tm_time = localtime(&time_box_time);
		if (tm_time != NULL) {
			lcd_printf(&time_box, "%d:%d:%d", tm_time->tm_hour, tm_time->tm_min, tm_time->tm_sec);
		}

  		get_local_ip(ip);
		lcd_printf(&ip_box, "%s", ip);	

		/*control screen update speed with
		 *UPDATE_SPEED definition
		 */
		if (spd_count >= UPDATE_SPEED) {
			lcd_update_screen();
			spd_count = 0;
		} else
			spd_count++;
	}

	lcd_fill_screen(COLOR_BLACK);
	lcd_update_screen();

	gpio_free(BTN_UP);
	gpio_free(BTN_MID);
	gpio_free(BTN_DOWN);

	lcd_free();
}

void get_local_ip(char *szip) {
	char *IP;
	int fd;
	struct ifreq ifr;

	fd = socket(AF_INET, SOCK_DGRAM, 0);

	ifr.ifr_addr.sa_family = AF_INET;

	strncpy(ifr.ifr_name, "wlan0", IFNAMSIZ-1);
	ioctl(fd, SIOCGIFADDR, &ifr);
	close(fd);

	IP = inet_ntoa(((struct sockaddr_in*) &ifr.ifr_addr)->sin_addr);
	strcpy(szip, IP);
}
