#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/processor.h>
#include <linux/init.h>
#include <linux/sched/task.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/jiffies.h>

static char *msg = "Guten tag!";
module_param(msg, charp, 0);

static u32 total_time_sec = 10;
module_param(total_time_sec, uint, 0);

static u32 msg_delay_sec = 1;
module_param(msg_delay_sec , uint, 0);

static u32 jfs_point;

static int msg_thread( void* data ) { 

        printk(KERN_INFO "%s\n", msg);

        while( !kthread_should_stop() ) {
        }

        return 0;
}

static int __init msg_module_init( void ) {
        struct task_struct *t;
        jfs_point = jiffies;

        printk(KERN_INFO "j1: %u, j2 %u", jfs_point, jiffies);

        do {
                t = kthread_run(msg_thread, NULL, "msg-thread%d");

                printk(KERN_INFO "Thread state: %ld",
                                t->state);

                msleep(msg_delay_sec * 1000);
                kthread_stop(t);

                printk(KERN_INFO "in cycle: "
                                "j1: %u, j2 %u", jfs_point, jiffies);
        } while ((jiffies - jfs_point) / HZ < total_time_sec);

                printk(KERN_INFO "out of cycle "
                                "j1: %u, j2 %u", jfs_point, jiffies);

        return 0; 
} 

static void __exit msg_module_exit(void)
{
        printk(KERN_INFO "msg-module: quit");
}

module_init(msg_module_init); 
module_exit(msg_module_exit);

MODULE_LICENSE( "GPL" );
