#!/bin/bash
#Note: I noticed no button bounce
echo 26 > /sys/class/gpio/export
echo in > /sys/class/gpio/gpio26/direction
echo out > /sys/class/gpio/gpio26/direction

val1=$(cat /sys/class/gpio/gpio26/value)

continue=1

while [ $continue == 1 ]
do
	if [[ "$val1" != "$val2" ]]; then
		if [ "$val1" == "1" ]; then
			sleep 1
			temp=$(cat /sys/class/gpio/gpio26/value)
			if [[ "$temp" == "1" ]];
			then
				continue=0
			else
				echo -n hours:
				i2cget -y 1 0x68 0x02
				echo -n min:
				i2cget -y 1 0x68 0x01
				echo -n sec:
				i2cget -y 1 0x68 0x00
				echo -n temp:
				i2cget -y 1 0x68 0x11
			fi
		fi
		val2=$val1
	fi
	val1=$(cat /sys/class/gpio/gpio26/value)
done
echo 26 > /sys/class/gpio/unexport
