#include <linux/fs.h>
#include <linux/init.h>
#include <linux/module.h>
#include <asm/uaccess.h>

#include <linux/stdio.h>

#define BUFFER_SIZE 1000

MODULE_LICENSE( "GPL" );
MODULE_AUTHOR( "Oleg Tsiliuric <olej@front.ru>" );
MODULE_VERSION( "6.3" );

static char msgr_buf[BUFFER_SIZE];

static ssize_t dev_read( struct file * file, char * buf,
                           size_t count, loff_t *ppos ) {
   int len = strlen( msgr_buf );
   printk( KERN_INFO "=== read : %ld\n", (long)count );
   if( count < len ) return -EINVAL;
   if( *ppos != 0 ) {
      printk( KERN_INFO "=== read return : 0\n" );  // EOF
      return 0;
   }
   if( copy_to_user( buf, msgr_buf, len ) ) return -EINVAL;
   *ppos = len;
   printk( KERN_INFO "=== read return : %d\n", len );
   return len;
}

static ssize_t dev_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset)
{
        char read_buf[100];
        size_t left;

        left = copy_from_user(read_buf, buffer, msg_length);

        strcat(msgr_buf, read_buf);

        return length;
}

static int __init dev_init( void );
module_init( dev_init );

static void __exit dev_exit( void );
module_exit( dev_exit );

