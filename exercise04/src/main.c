#include  <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "game.h"

void ReadName(char* const name);
void SetNames();


int main(int argc, char *argv[])
{
        int plcount = 2;
        for (int i = 1; i + 1 <= argc; i++) {
                if (argv[i][0] == '-' && argv[i][1] =='p') {
                        int temp = atoi(argv[i+1]);
                        if (temp >= 2)
                                plcount = temp;
                }
                if (argv[i][0] == '-' && argv[i][1] =='g') {
                        if (!strcmp(argv[i+1], "multiplayer")) 
                                gametype =  MULTIPLAYER;
                        else if (!strcmp(argv[i+1], "singleplayer"))
                                gametype =  SINGLEPLAYER;
                }

        }

        game = galloc(plcount);

        GameInit();

        SetNames();

        while (gameOver) {

                Round();

        }
}

void ReadName(char* const name)
{
        int i = 0, c;
        while ((c = getchar()) != EOF && c != '\n' && i+1 < NAME_SIZE)
                name[i++] = c;
        name[i] = '\0';
}

/*      Set names for all players in GAME
 *      Tip: No check that player dont have same name
 */
void SetNames()
{
        for (int i = 0; i < game->plcount; i++)
        {
                t_player *pl = game->players[i];
                if (pl->type == HUMAN) {
                        printf("Player %d, please enter your name\n", i);
                        ReadName(pl->name);
                } else {
                        sprintf(pl->name, "Player-PC%d", i);
                }
        }
}
