
extern int DicesAmount;
extern int RoundNumber;
extern int gameOver;

#define NAME_SIZE 20
#define MAXPOINTS 36

enum GAMETYPE {
        SINGLEPLAYER,
        MULTIPLAYER
};
extern enum GAMETYPE gametype;

enum PLAYERTYPE {
        HUMAN,
        PC
};

typedef struct PLAYER {
        char *name;
        int id;
        enum PLAYERTYPE type;
        int points;
} t_player;

/*Linked list for saving game result.
 *Each round result is stored in array.
 */
struct round {
        int *players_result;
        struct round *next;
        struct round *prev;
};

struct GAME {
        t_player **players;
        struct round *first;
        int plcount;
};
extern struct GAME *game;

t_player *palloc(void);
struct GAME *galloc(int plcount);
int ThrowDices(void);
void GameInit();
void Round();
