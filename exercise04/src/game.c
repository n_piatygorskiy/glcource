#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "game.h"

int DicesAmount = 2;
int RoundNumber = 0;
int gameOver = 1;

enum GAMETYPE gametype = SINGLEPLAYER;
struct GAME *game;

int getch(void);
void SaveResult();
void PrintResult();

t_player *palloc(void)
{
        t_player *p = (t_player*)malloc(sizeof(t_player));
        p->name = (char *)malloc(NAME_SIZE*sizeof(char));
        return p;
}

struct GAME *galloc(int plcount)
{
        struct GAME *p = (struct GAME*)malloc(sizeof(struct GAME));
        p->players = (t_player**)malloc(plcount*sizeof(t_player*));
        p->plcount = plcount;

        return p;
}

int ThrowDices(void)
{
        static int srandinit = 0;

        if (!srandinit) {
                srand(time(NULL));
                srandinit = 1;
        }

        int res = 0;
        for (int i = 0; i < DicesAmount; i++){
        res += rand() % 6 + 1;
        }
        return res;
}

/*      Alloc and init players in GAME
 */
void GameInit()
{
        for (int i = 0; i < game->plcount; i++)
        {
                game->players[i] = palloc();
                game->players[i]->id = i;
                if (gametype == SINGLEPLAYER && i > 0)
                        game->players[i]->type = PC;
                else
                        game->players[i]->type = HUMAN;
        }
}

void Round()
{
        int minres = MAXPOINTS;
        t_player *winner = NULL;
        printf("Round %d\n", RoundNumber);
        for (int i = 0; (i < game->plcount) && gameOver; i++)
        {
                t_player *pl = game->players[i];
                if (pl->type == HUMAN)
                {
                        char c;
                        printf("%s, press <ENTER> to throw dices\n", pl->name);
                        while ((c = getch()) != '\n' && c != '\e') {
                                if (c == 'i')
                                        PrintResult();
                        }

                        if (c == '\e')
                                gameOver = 0;
                }
                if (gameOver) {
                        int res = ThrowDices();
                        if (minres > res)
                                minres = res;

                        pl->points = res;

                        printf("%s got %d points!\n",pl->name, pl->points);

                        if (winner != NULL) {
                                if (winner->points < pl->points)
                                        winner = pl;
                        } else
                                winner = pl; 
                }
        }
        if (gameOver) {
                if (minres == winner->points)
                        puts("Draw: no winner");
                else
                        printf("Winner is playes %d - %s\n", winner->id, winner->name);
        }
        SaveResult();
        RoundNumber++;
}

void SaveResult()
{
        struct round *last = game->first;
        struct round *prev = NULL;

        while (last != NULL) {
                prev = last;
                last = last->next;
        }

        last = (struct round*) malloc(sizeof(*last));
        last->players_result = (int *) malloc(sizeof(int)*game->plcount);
        last->prev = prev;
        if (prev != NULL)
                prev->next = last;

        if (game->first == NULL)
                game->first = last;

        for (int i = 0; i < game->plcount; i++)
                last->players_result[i] = game->players[i]->points;
}

void PrintResult()
{
        struct round *rnd = game->first;
        for (int i = 0; rnd != NULL; i++) {
                if (i == 0)
                        puts("Game Result!");
                printf("Round %d:\n", i);
                for (int j = 0; j < game->plcount; j++)
                        printf("%s got %d points\n", game->players[j]->name, rnd->players_result[j]);
                rnd = rnd->next;
        }
}
