# Exercise04: dice game
Simple console dice game. Has two modes: singleplayer and multiplayer.
Amount of players and game type could be choosen by command line parameters. Also there is small hotkey menu inside game.
## Comimand line parameters
**-p amount** choose amount of players \
**-g {singleplayer, muliplayer}** choose game type \
*Tip: in singleplayer game "bots" choose names for themselves and not wait for command to throw dices*
## Ingame menu
**esc** Quit the game \
**i** Show game history \
