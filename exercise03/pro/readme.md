# First subtask
git config --global user.name "Nikita"  
git config --global user.email "n_piatygorskiy@knu.ua"  
git config --global core.autocrlf input  
git config --global core.safecrlf true  
# Second subtask
mkdir exercise03  
cd exercise03  
# Thirs subtask
mkdir pro  
cd pro  
touch readme.md  
git add .  
git commit -m "repo: initial commit"  
# Fourth subtask
git checkout -b first_branch  
vim readme.md  
git status  
git commit -am "readme: add the command log of the 1st subtask"  
# Fifth subtask
git checkout master  
vim readme.md  
git commit -am "readme: add command log to solve 2nd subtask"  
git log --oneline --decorate --graph --all  
# Sixth subtask
git status  
git merge first_branch  
git mergetool  
git status  
git log --oneline --decorate --graph --all  
# Seventh subtask
vim readme.md  
git commit -am "readme: add command log to solve 3rd subtask"  
vim readme.md  
git commit -am "readme: add command log to solve 4th subtask"  
vim readme.md  
git commit -am "readme: add command log to solve 5th subtask"  
vim readme.md  
git commit -am "readme: add command log to solve 6th subtask"  
vim readme.md  
git commit -am "readme: add command log to solve 7th subtask"  
git log --oneline --decorate --graph --all  

