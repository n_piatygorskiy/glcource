#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/hrtimer.h>
#include <linux/ktime.h>

MODULE_LICENSE("GPL");

/*I have changed interval_ns value to 1 sec,
 * now every second i can see in dmesg 'Timer
 * end'
 */

unsigned long timer_interval_ns = 1e9;
static struct hrtimer hr_timer;

/*callback function invoken, each time the timer
 *reaches period of timer_interval_ns
 */
enum hrtimer_restart timer_callback( struct hrtimer *timer_for_restart )
{
  	ktime_t currtime , interval;
  	currtime  = ktime_get();
  	interval = ktime_set(0,timer_interval_ns); 
  	hrtimer_forward(timer_for_restart, currtime , interval);
        //signal, that timer has eneded
        printk(KERN_NOTICE "Timer end!\n");
	return HRTIMER_RESTART;
}

static int __init timer_init(void) {
	ktime_t ktime = ktime_set( 0, timer_interval_ns );
	hrtimer_init( &hr_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL );
	hr_timer.function = &timer_callback;
 	hrtimer_start( &hr_timer, ktime, HRTIMER_MODE_REL );
	return 0;
}

static void __exit timer_exit(void) {
	int ret;
  	ret = hrtimer_cancel( &hr_timer );
  	if (ret) printk("The timer was still in use...\n");
  	printk("HR Timer module uninstalling\n");
	
}

module_init(timer_init);
module_exit(timer_exit);

