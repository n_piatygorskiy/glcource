#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define ENC_SIZE 1000

int main(void)
{
  char *szenc = (char *)malloc(ENC_SIZE);
  szenc[0] = 0;
  char num[100];
  int iencsize = ENC_SIZE;

  char c, oldc = getchar();
  int i = 0, n = 1;;
  while ((c = getchar()) != EOF)
  {
    if (c == oldc)
      n++;
    else {
      if (n > 1)
      {
        sprintf(num,"%d",n);
        n = 1;
        int l = strlen(num);
        if (i + l >= iencsize) {
          szenc = realloc(szenc, iencsize*2);
          iencsize *= 2;
        }
        strcat(szenc, num);
        i += l;
      }
      if (i + 4 >= iencsize) {
        szenc = realloc(szenc, iencsize*2);
        iencsize *= 2;
      }
      if ((oldc == '\\') || (oldc >= '0' && oldc <= '9'))
        szenc[i++] = '\\';
      szenc[i++] = oldc;
      szenc[i] = '\0';
    }
    oldc = c;
  }
  if (c == oldc)
      n++;
  else {
      if (n > 1)
      {
        sprintf(num,"%d",n);
        n = 1;
        int l = strlen(num);
        if (i + l >= iencsize) {
          szenc = realloc(szenc, iencsize*2);
          iencsize *= 2;
        }
        strcat(szenc, num);
        i += l;
      }
      if (i + 4 >= iencsize) {
        szenc = realloc(szenc, iencsize*2);
        iencsize *= 2;
  }
  if ((oldc == '\\') || (oldc >= '0' && oldc <= '9'))
      szenc[i++] = '\\';
      szenc[i++] = oldc;
      szenc[i] = '\0';
  }

  szenc[i] = '\0';
  printf("%s", szenc);
  free(szenc);
}

