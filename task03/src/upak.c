#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define ENC_SIZE 1000

int main(void)
{
  char *szenc = (char *)malloc(ENC_SIZE);
  szenc[0] = 0;
  char num[100], c;
  num[0] = 0;
  int encsize = ENC_SIZE;

  int i = 0;
  for (int in=0 ; (c = getchar()) != EOF; ) {
    if (c >= '0' && c <= '9')
      num[in++] = c;
    else {
      int n = 1;
      if (num[0] != 0)
        n = atoi(num);
      if (c == '\\')
        c = getchar();
      if (i + n + 2>= encsize) {
              encsize *= 2;
              szenc = realloc(szenc, encsize);
      }
      for (int j = 0; j < n; j++)
        szenc[i++] = c;
      in = 0;
    }
    num[in] = 0;
  }
  szenc[i] = 0;
  printf("%s", szenc);

}
