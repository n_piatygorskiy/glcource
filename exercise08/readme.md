# Instruction: how to build and install test linux test system for Debian.
## Getting the kernel sources
git clone https://github.com/torvalds/linux
## Configuring
* Prepare build directory \
        export BUILD_KERNEL=~/Drive/src/kernel/build \
        sudo apt-get install bison \
        cd ~/Drive/src/kernel/linux \
        (linux - path where sources were placed) \
        make ARCH=x85_64 O=${BUILD_KERNEL} defconfig \
        cd ${BUILD_KERNEL}
* Configure kernel
        make menuconfig \
        leave everything as is and press <esc><esc><enter>
## Building kernel
        make -j4 \
        in case of errors try to install this packages: \
        sudo apt-get install libssl-dev \
        sudo apt-get install libelf-dev
## Building rootfs
* Get buildroot sources
        git clone https://github.com/buildroot/buildroot
* Prepare build directory
        export BUILD_ROOTFS=~/Drive/src/kernel/buildrootfs \
        make O=${BUILD_ROOTFS} qemu_x86_64_defconfig \
        cd ${BUILD_ROOTFS}
* Configure buildroot
        make menuconfig \
        target architecture = x86_64 \
        header series = 5.10.x or later \
        + enable wchar support \
        system configuration ... \
        - Linux Kernel
* Additional files
        echo "user 1000 user 1000 =pass /home/user /bin/bash - Linux User" > ${BUILD_ROOTFS}/users \
        mkdir -p ${BUILD_ROOTFS}/root/etc/sudoers.d \
        echo "userALL=(ALL)ALL" > ${BUILD_ROOTFS}/root/etc/sudoers.d/user \
        echo "/bin/sh" > ${BUILD_ROOTFS}/root/etc/shells \
        echo "/bin/bash" >> ${BUILD_ROOTFS}/root/etc/shells
* Build the rootfs
        make -j4
## Launch qemu
### Install kvm
### Launch
sudo qemu-system-x86_64 \-kernel ${BUILD_KERNEL}/arch/x86/boot/bzImage \-append "root=/dev/sda console=ttyS0" \-hda ${BUILD_ROOTFS}/images/rootfs.ext3 -device e1000,netdev=net0 -netdev user,id=net0,hostfwd=tcp::8022-:22
### And login via ssh
ssh -p 8022 user@localhost \
$ uname -r \
5.10.0
