#!/bin/bash

#------------------------
#pause between blinking in ms
tblink=500
#-------------------------

function blink(){
#save btn state
val2=$val1
#rgb state
state=0
#for timing
tprev=$(date +%s%N | cut -b1-13)
while [ 1 ]
do
	if [[ "$reverse" == "1" ]]
	then
		case "$state" in
			"0" )	echo 0 > /sys/class/gpio/gpio21/value
				echo 1 > /sys/class/gpio/gpio16/value
				;;
			"1" )	echo 0 > /sys/class/gpio/gpio16/value
				echo 1 > /sys/class/gpio/gpio20/value
				;;
			"2" )	echo 0 > /sys/class/gpio/gpio20/value
				echo 1 > /sys/class/gpio/gpio21/value
				;;
		esac
	else
		case "$state" in
			"0" )	echo 0 > /sys/class/gpio/gpio21/value
				echo 1 > /sys/class/gpio/gpio16/value
				;;
			"1" )	echo 0 > /sys/class/gpio/gpio20/value
				echo 1 > /sys/class/gpio/gpio21/value
				;;
			"2" )	echo 0 > /sys/class/gpio/gpio16/value
				echo 1 > /sys/class/gpio/gpio20/value
				;;
		esac
	fi
	#Check timing
	tcur=$(date +%s%N | cut -b1-13)
	dt=$((tcur-tprev))
	if [[ $dt -gt $tblink ]]; 
	then
		if (( state < 2));
		then
			((state++))
		else
			state=0
		fi
		tprev=$tcur
	fi
	#Check button
	if [[ "$val1" != "$val2" ]]; then
		#If button was pressed down
		if [ "$val1" == "1" ]; then
			if [ "$reverse" == "1" ]	
			then
				reverse=""
			else
				reverse=1
			fi
		fi
		#old button state
		val2=$val1
	fi
	#Refresh current btn state value
	val1=$(cat /sys/class/gpio/gpio26/value)

done
}

#Initilize rgb module
echo 16 > /sys/class/gpio/export
echo 20 > /sys/class/gpio/export
echo 21 > /sys/class/gpio/export

echo out > /sys/class/gpio/gpio16/direction
echo out > /sys/class/gpio/gpio20/direction
echo out > /sys/class/gpio/gpio21/direction

#Initilaze button
echo 26 > /sys/class/gpio/export
echo in > /sys/class/gpio/gpio26/direction

#save current button state
val1=$(cat /sys/class/gpio/gpio26/value)
count=0

while [ 1 ]
do
	if [[ "$val1" != "$val2" ]]; then
		#If button was pressed down
		if [ "$val1" == "1" ]; then
			blink
		fi
		#old button state
		val2=$val1
	fi
	#Refresh current btn state value
	val1=$(cat /sys/class/gpio/gpio26/value)
done


#Free rgb module
echo 16 > /sys/class/gpio/unexport
echo 20 > /sys/class/gpio/unexport
echo 21 > /sys/class/gpio/unexport

#Free button
echo 26 > /sys/class/gpio/unexport
