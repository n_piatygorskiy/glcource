#!/bin/bash
#Note: I noticed no button bounce
echo 26 > /sys/class/gpio/export
echo in > /sys/class/gpio/gpio26/direction
echo out > /sys/class/gpio/gpio26/direction

val1=$(cat /sys/class/gpio/gpio26/value)

count=0

while [ 1 ]
do
	if [[ "$val1" != "$val2" ]]; then
		if [ "$val1" == "1" ]; then
			(( count++ ))
			echo Count: $count
		fi
		val2=$val1
	fi
	val1=$(cat /sys/class/gpio/gpio26/value)
done
echo 26 > /sys/class/gpio/unexport
