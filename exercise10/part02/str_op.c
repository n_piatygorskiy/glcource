#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>

int flip_words(char *str);
void to_upcase(char *str);

/*operation type: 
 *      0 - flip words
 *      1 - make str upcase
 */
static int op_type=0;
module_param(op_type,int,0660);

static char *input_str="reverse me";
module_param(input_str,charp,0660);

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Nikita Piatygorskiy");

static int simple_init(void)
{
        if (op_type == 0)
                flip_words(input_str);
        else
                to_upcase(input_str);
        printk(KERN_INFO "%s", input_str);
	return 0;
}

static void simple_cleanup(void)
{
}

module_init(simple_init);
module_exit(simple_cleanup);

int flip_words(char *szstr)
{
        int isspace(int c);
        int ps = 0, words = 0;

        while (szstr[ps] != '\0') {
                if (!isspace(szstr[ps])) {
                        int wbeg = ps;
                        int wend = ps;
                        while (!isspace(szstr[wend+1]) && szstr[wend+1] != '\0')
                                wend++;
                        ps = wend;
                        while (wbeg < wend)
                        {
                                int temp = szstr[wbeg];
                                szstr[wbeg++] = szstr[wend]; 
                                szstr[wend--] = temp;
                        }
                        words++;
                }
                ps++;
        }
        return words;
}

void to_upcase(char *szstr)
{
        int ps = 0, c;
        for ( ; (c = szstr[ps]) != '\0'; ps++) {
                if (c >= 'a' && c <= 'z')
                        szstr[ps] -= 'a' - 'A';
        }
}

int isspace(int c)
{
        return c == ' ' ||
                c == '\t' ||
                c == '\n' ||
                c == '\b' ||
                c == '\r' ||
                c == '\v';
}
