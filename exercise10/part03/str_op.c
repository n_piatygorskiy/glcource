#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/pci.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>

#include <linux/string.h>

MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Nikita Piatygorskiy <n_piatygorskiy@knu.ua>");
MODULE_DESCRIPTION("format string to upcase/flip words");
MODULE_VERSION("0.1");

#define MODULE_TAG      "converter_module "
#define PROC_DIRECTORY  "converter"
#define PROC_FILENAME   "buffer"
#define BUFFER_SIZE     100
#define LEN_MSG         100

static char buf_msg[LEN_MSG];

static ssize_t user_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset);
static ssize_t user_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset);
int flip_words(char *str);
void to_upcase(char *str);

static char *proc_buffer;
static size_t proc_msg_length;
static size_t proc_msg_read_pos;

static struct proc_dir_entry *proc_dir;
static struct proc_dir_entry *proc_file;

static struct proc_ops proc_fops = {
        .proc_read = user_read,
        .proc_write = user_write,
};

static ssize_t user_read(struct file *file_p, char __user *buffer, size_t length, loff_t *offset)
{
        size_t left;

        if (length > (proc_msg_length - proc_msg_read_pos))
                length = (proc_msg_length - proc_msg_read_pos);

        if (strcmp(buf_msg, "1\n") == 0)
                flip_words(proc_buffer);
        else if (strcmp(buf_msg,"2\n") == 0)
                to_upcase(proc_buffer);
        else if (strcmp(buf_msg, "0\n") != 0)
                printk(KERN_NOTICE MODULE_TAG "Invalid conv value, expected [0-2], got %s", buf_msg);

        left = copy_to_user(buffer, &proc_buffer[proc_msg_read_pos], length);

        proc_msg_read_pos += length - left;

        if (left)
                printk(KERN_ERR MODULE_TAG "failed to read %u from %u chars\n", left, length);
        else
                printk(KERN_NOTICE MODULE_TAG "read %u chars\n", length);

        return length - left;

        return 0;
}

static ssize_t user_write(struct file *file_p, const char __user *buffer, size_t length, loff_t *offset)
{
        size_t msg_length;
        size_t left;

        if (length > BUFFER_SIZE)
        {
                printk(KERN_WARNING MODULE_TAG "reduse message length from %u to %u chars\n", length, BUFFER_SIZE);
                msg_length = BUFFER_SIZE;
        }
        else
                msg_length = length;

        left = copy_from_user(proc_buffer, buffer, msg_length);

        proc_msg_length = msg_length - left;
        proc_msg_read_pos = 0;

        if (left)
                printk(KERN_ERR MODULE_TAG "failed to write %u from %u chars\n", left, msg_length);
        else
                printk(KERN_NOTICE MODULE_TAG "written %u chars\n", msg_length);

        return length;

        return length;
}

static ssize_t msg_show(struct class *class, struct class_attribute *attr, char *buf)
{
   strcpy( buf, buf_msg );
   printk( "read %ld\n", (long)strlen( buf ) );
   return 0;
}

static ssize_t msg_store(struct class *class, struct class_attribute *attr, const char *buf, size_t count)
{
   printk( "write %ld\n", (long)count );
   strncpy( buf_msg, buf, count );
   buf_msg[ count ] = '\0';
   return count;
}

struct class_attribute class_attr_conv = __ATTR(conv, (S_IWUSR | S_IRUGO), &msg_show, &msg_store);

static int create_buffer(void)
{
    proc_buffer = kmalloc(BUFFER_SIZE, GFP_KERNEL);
    if (NULL == proc_buffer)
        return -ENOMEM;
    proc_msg_length = 0;

    return 0;
}


static void cleanup_buffer(void)
{
    if (proc_buffer) {
        kfree(proc_buffer);
        proc_buffer = NULL;
    }
    proc_msg_length = 0;
}


static int create_converter_proc(void)
{
    proc_dir = proc_mkdir(PROC_DIRECTORY, NULL);
    if (NULL == proc_dir)
        return -EFAULT;

    proc_file = proc_create(PROC_FILENAME, S_IFREG | S_IRUGO | S_IWUGO, proc_dir, &proc_fops);
    if (NULL == proc_file)
        return -EFAULT;

    return 0;
}

static void cleanup_converter_proc(void)
{
    if (proc_file)
    {
        remove_proc_entry(PROC_FILENAME, proc_dir);
        proc_file = NULL;
    }
    if (proc_dir)
    {
        remove_proc_entry(PROC_DIRECTORY, NULL);
        proc_dir = NULL;
    }
}

static struct class *msg_class;

static int simple_init(void)
{
       int err;

        msg_class = class_create(THIS_MODULE, "msg-class");

        if(IS_ERR(msg_class))
                printk("bad class create\n");
        err = class_create_file(msg_class, &class_attr_conv);

        err = create_buffer();
        if (err)
                goto error;

        err = create_converter_proc();
        if (err)
                 goto error;

        printk(KERN_NOTICE MODULE_TAG "loaded\n");
        return 0;

error:
        printk(KERN_ERR MODULE_TAG "failed to load\n");
        cleanup_converter_proc();
        cleanup_buffer();
        return err;
}

static void simple_cleanup(void)
{
        cleanup_converter_proc();
        class_remove_file( msg_class, &class_attr_conv );
        class_destroy( msg_class );
        cleanup_buffer();
        printk(KERN_NOTICE MODULE_TAG "exited\n");
}

module_init(simple_init);
module_exit(simple_cleanup);

int flip_words(char *szstr)
{
        int isspace(int c);
        int ps = 0, words = 0;

        while (szstr[ps] != '\0') {
                if (!isspace(szstr[ps])) {
                        int wbeg = ps;
                        int wend = ps;
                        while (!isspace(szstr[wend+1]) && szstr[wend+1] != '\0')
                                wend++;
                        ps = wend;
                        while (wbeg < wend)
                        {
                                int temp = szstr[wbeg];
                                szstr[wbeg++] = szstr[wend]; 
                                szstr[wend--] = temp;
                        }
                        words++;
                }
                ps++;
        }
        return words;
}

void to_upcase(char *szstr)
{
        int ps = 0, c;
        for ( ; (c = szstr[ps]) != '\0'; ps++) {
                if (c >= 'a' && c <= 'z')
                        szstr[ps] -= 'a' - 'A';
        }
}

int isspace(int c)
{
        return c == ' ' ||
                c == '\t' ||
                c == '\n' ||
                c == '\b' ||
                c == '\r' ||
                c == '\v';
}
